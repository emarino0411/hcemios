<?php

use Illuminate\Database\Migrations\Migration;

class CreateCheckvoucherTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('check_voucher', function($table) {
            $table->increments('id');
            $table->integer('po_no')->nullable();
            $table->date('issue_date')->nullable();
            $table->date('release_date')->nullable();
            $table->integer('representative_id');
            $table->date('check_date')->nullable();
            $table->decimal('check_amount', 5, 0)->nullable();
            $table->decimal('account_deduction', 5, 0)->nullable();
            $table->decimal('wsp_deduction', 5, 0)->nullable();
            $table->decimal('other_deduction', 5, 0)->nullable();
            $table->integer('user_id')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('check_voucher');
    }

}