<?php

use Illuminate\Database\Migrations\Migration;

class CreateCommissionsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commissions', function($table) {
            $table->increments('id');
            $table->integer('po_no')->nullable();
            $table->integer('representative_id')->nullable();
            $table->date('due_date')->nullable();
            $table->string('percentage', 45)->nullable();
            $table->string('status', 45)->nullable();
            $table->string('wsp_percentage', 45)->nullable();
            $table->string('account_balance_percentage', 45)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('commissions');
    }

}