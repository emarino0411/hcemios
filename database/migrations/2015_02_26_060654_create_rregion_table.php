<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRregionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rregion', function(Blueprint $table)
		{
            $table->increments('id');
            $table->string('regcode', 2);
            $table->string('regname', 50);
            $table->string('nscb_reg_code', 9);
            $table->string('nscb_reg_name', 100);
            $table->string('addedby', 50);
            $table->integer('UserLevelID');
            $table->dateTime('dateupdated');
            $table->char('status',1);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rregion');
	}

}
