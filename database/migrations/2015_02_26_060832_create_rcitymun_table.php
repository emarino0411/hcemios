<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRcitymunTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rcitymun', function(Blueprint $table)
		{
            $table->increments('id');
            $table->string('regcode', 2);
            $table->string('provcode', 4);
            $table->string('citycode', 6);
            $table->string('cityname', 100);
            $table->string('nscb_city_code', 9);
            $table->string('nscb_city_name', 100);
            $table->string('addedby', 50);
            $table->integer('UserLevelID');
            $table->dateTime('dateupdated');
            $table->char('status',1);

            $table->index('regcode');
            $table->index('provcode');
            $table->index('citycode');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rcitymun');
	}

}
