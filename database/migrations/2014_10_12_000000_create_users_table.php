<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('users', function (Blueprint $table) {
      $table->increments('id');
      $table->string('email')->unique();
      $table->string('password', 60)->nullable();
      $table->string('last_name', 45)->nullable();
      $table->string('first_name', 45)->nullable();
      $table->string('gender', 45)->nullable();
      $table->string('civil_status', 45)->nullable();
      $table->string('birthdate', 45)->nullable();
      $table->string('landline', 45)->nullable();
      $table->string('mobile', 45)->nullable();
      $table->string('street', 45)->nullable();
      $table->string('brgy', 45)->nullable();
      $table->string('city', 45)->nullable();
      $table->string('province', 45)->nullable();
      $table->string('status')->nullable();
      $table->integer('user_role')->index();

      $table->rememberToken();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('users');
  }
}
