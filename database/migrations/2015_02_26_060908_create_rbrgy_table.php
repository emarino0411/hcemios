<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRbrgyTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('rbrgy', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('regcode', 2);
            $table->string('provcode', 4);
            $table->string('citycode', 6);
            $table->string('bgycode', 9);
            $table->string('bgyname', 100);
            $table->string('nscb_brgy_code', 9);
            $table->string('nscb_brgy_name', 100);
            $table->string('addedby', 50);
            $table->integer('UserLevelID');
            $table->dateTime('dateupdated');
            $table->char('status',1);

            $table->index('regcode');
            $table->index('provcode');
            $table->index('citycode');
            $table->index('bgycode');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('rbrgy');
	}

}
