<?php

use Illuminate\Database\Migrations\Migration;

class CreatePurchaseTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase', function($table) {
            $table->increments('id');
            $table->date('purchase_date');
            $table->integer('terms');
            $table->date('delivery_date');
            $table->float('total_amount');
            $table->integer('associate_id')->index();
            $table->integer('sponsor_id');
            $table->integer('consultant_id')->index();
            $table->string('po_no', 45)->nullable();
            $table->string('transaction_status', 45);
            $table->string('fast_track', 45);
            $table->string('status', 45);
            $table->string('balance', 45);
            $table->string('remarks', 45);
            $table->integer('production_points')->nullable();
            $table->integer('client_client_id')->index();
            $table->integer('user_user_id')->index();

            $table->timestamp('updated_at')->nullable();
            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('purchase');
    }

}