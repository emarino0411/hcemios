<?php

use Illuminate\Database\Migrations\Migration;

class CreateRepresentativeTable extends Migration
{

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('representative', function ($table) {
      $table->increments('id');
      $table->string('representative_code', 45)->unique();
      $table->string('last_name', 45)->nullable();
      $table->string('first_name', 45)->nullable();
      $table->string('middle_name', 45)->nullable();
      $table->string('gender', 45)->nullable();
      $table->string('profile_picture', 45)->nullable();
      $table->string('sponsor_id', 45)->nullable();
      $table->string('sponsor_date')->nullable();
      $table->string('distributor_id', 45)->nullable();
      $table->string('phase1_start')->nullable();
      $table->string('phase1_end')->nullable();
      $table->string('phase2_start')->nullable();
      $table->string('phase2_end')->nullable();
      $table->string('birthdate')->nullable();
      $table->string('birthplace', 45)->nullable();
      $table->string('civil_status', 45)->nullable();
      $table->string('street', 45)->nullable();
      $table->string('city', 45)->nullable();
      $table->string('brgy', 45)->nullable();
      $table->string('province', 45)->nullable();
      $table->string('address_province', 100)->nullable();
      $table->string('phone1', 45)->nullable();
      $table->string('phone2', 45)->nullable();
      $table->string('phone3', 45)->nullable();
      $table->string('email_address', 45)->nullable();
      $table->string('tin_no', 45)->nullable();
      $table->string('sss_no', 45)->nullable();
      $table->string('philhealth_no', 45)->nullable();
      $table->string('pagibig_no', 45)->nullable();
      $table->string('can_drive', 45)->nullable();
      $table->string('own_car', 45)->nullable();
      $table->string('previous_employer', 45)->nullable();
      $table->string('p_employer_telno', 45)->nullable();
      $table->string('p_employer_addr', 45)->nullable();
      $table->string('p_employer_email', 45)->nullable();
      $table->string('spouse_last_name', 45)->nullable();
      $table->string('spouse_first_name', 45)->nullable();
      $table->string('no_of_dependents', 45)->nullable();
      $table->string('active', 45)->nullable();
      $table->string('level_id', 45)->nullable();
      $table->string('level_start')->nullable();
      $table->string('signature', 45)->nullable();
      $table->string('total_production', 45)->nullable();
      $table->string('total_account_balance', 45)->nullable();
      $table->string('total_wsp', 45)->nullable();
      $table->string('status', 45)->default('ACTIVE');


      $table->timestamp('updated_at')->nullable();
      $table->timestamp('created_at')->nullable();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('representative');
  }

}