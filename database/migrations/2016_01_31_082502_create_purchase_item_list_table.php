<?php

use Illuminate\Database\Migrations\Migration;

class CreatePurchaseitemlistTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_item_list', function($table) {
            $table->increments('id');
            $table->string('gift', 45)->nullable();
            $table->string('item_price', 45)->nullable();
            $table->integer('item_id')->index();
            $table->integer('purchase_po_no')->index();
            $table->integer('user_user_id')->index();
            $table->string('purchase_type')->nullable();
            $table->integer('qty')->nullable();


            $table->timestamp('updated_at')->nullable();
            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('purchase_item_list');
    }

}