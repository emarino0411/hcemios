<?php

use Illuminate\Database\Migrations\Migration;

class CreateItemTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item', function($table) {
            $table->increments('id');
            $table->string('item_code', 45)->unique();
            $table->string('description', 45)->nullable();
            $table->string('retail_price', 45)->nullable();
            $table->string('inventory_price', 45)->nullable();
            $table->string('production_count', 45)->nullable();
            $table->string('production_count_FT', 45)->nullable();
            $table->string('product_type', 45)->nullable();
            $table->string('category', 45)->nullable();
            $table->string('type', 45)->nullable();
            $table->string('status', 45)->default('Active');

            $table->timestamp('updated_at')->nullable();
            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('item');
    }

}