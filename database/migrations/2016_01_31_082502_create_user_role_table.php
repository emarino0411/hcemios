<?php

use Illuminate\Database\Migrations\Migration;

class CreateUserRoleTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function($table) {
            $table->increments('id');
            $table->string('name', 45)->nullable();
            $table->string('description', 45)->nullable();

            $table->integer('purchase_view')->default('0');
            $table->integer('purchase_add')->default('0');
            $table->integer('purchase_edit')->default('0');
            $table->integer('purchase_delete')->default('0');


            $table->integer('payments_view')->default('0');
            $table->integer('payments_add')->default('0');
            $table->integer('payments_edit')->default('0');
            $table->integer('payments_delete')->default('0');

            $table->integer('bookings_view')->default('0');
            $table->integer('bookings_add')->default('0');
            $table->integer('bookings_edit')->default('0');
            $table->integer('bookings_delete')->default('0');


            $table->integer('agents_view')->default('0');
            $table->integer('agents_add')->default('0');
            $table->integer('agents_edit')->default('0');
            $table->integer('agents_delete')->default('0');


            $table->integer('clients_view')->default('0');
            $table->integer('clients_add')->default('0');
            $table->integer('clients_edit')->default('0');
            $table->integer('clients_delete')->default('0');



            $table->integer('users_view')->default('0');
            $table->integer('users_add')->default('0');
            $table->integer('users_edit')->default('0');
            $table->integer('users_delete')->default('0');



            $table->integer('roles_view')->default('0');
            $table->integer('roles_add')->default('0');
            $table->integer('roles_edit')->default('0');
            $table->integer('roles_delete')->default('0');


            $table->integer('promos_view')->default('0');
            $table->integer('promos_add')->default('0');
            $table->integer('promos_edit')->default('0');
            $table->integer('promos_delete')->default('0');

            $table->integer('reports_view')->default('0');
            $table->integer('reports_add')->default('0');
            $table->integer('reports_edit')->default('0');
            $table->integer('reports_delete')->default('0');

            $table->integer('distribute_commission')->default('0');
            $table->integer('print_voucher')->default('0');
            $table->integer('print_check')->default('0');

            $table->integer('dashboard_new_po')->default('0');
            $table->integer('dashboard_new_agent')->default('0');
            $table->integer('dashboard_bookings')->default('0');
            $table->integer('dashboard_due_payments')->default('0');
            $table->integer('dashboard_approved_commissions')->default('0');
            $table->integer('dashboard_promo_qualifiers')->default('0');
            $table->integer('dashboard_for_pull_out')->default('0');
            $table->integer('dashboard_checks_follow_up')->default('0');
            $table->integer('dashboard_calendar')->default('0');
            $table->integer('dashboard_orders')->default('0');
            $table->integer('dashboard_top_agents')->default('0');


            $table->timestamp('updated_at')->nullable();
            $table->timestamp('created_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('roles');
    }

}