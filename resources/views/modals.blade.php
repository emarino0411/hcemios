<div id="search-purchase" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-blue">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="ion ion-search"></i> Search Purchase Order</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-10">
            <!-- form start -->
            <form class="form-horizontal">
              <div class="box-body">

                <!-- Date Range -->
                <div class="form-group" id="date">
                  <label for="txt_daterange" class="col-sm-3 control-label">Date Range</label>
                  <div class="col-sm-8">
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" class="form-control pull-right" id="txt_daterange">
                      </div>
                      
                      <!-- /.input group -->
                  </div>
                  <label for="txt_daterange" class="control-label"><a class="hide-filter" style="cursor:pointer;"><i class="fa fa-times"></i></a></label>
                  
                </div>
                <!-- /Date Range -->

                <!-- Client -->
                <div class="form-group" id="client">
                  <label for="txt_client" class="col-sm-3 control-label">Client Name</label>
                  <div class="col-sm-8">
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-user"></i>
                        </div>
                        <input type="text" class="form-control pull-right" id="txt_client">
                      </div>
                      <!-- /.input group -->
                  </div>
                  <label for="txt_client" class="control-label"><a class="hide-filter" style="cursor:pointer;"><i class="fa fa-times"></i></a></label>
                  
                </div>
                <!-- / Client -->

                <!-- PO No -->
                <div class="form-group" id="po_no">
                  <label for="txt_po_no" class="col-sm-3 control-label">PO Number</label>

                  <div class="col-sm-8">
                      <div class="input-group">
                        <div class="input-group-addon">
                          <i class="fa fa-ellipsis-h "></i>
                        </div>
                        <input type="text" class="form-control pull-right" id="txt_po_no">
                      </div>
                      <!-- /.input group -->
                  </div>
                  <label for="txt_po_no" class="control-label"><a class="hide-filter" style="cursor:pointer;"><i class="fa fa-times"></i></a></label>
                  
                </div>
                <!-- / PO No -->

                <!-- Purchased Item -->
                <div class="form-group" id="item">
                  <label for="slct_items" class="col-sm-3 control-label">Purchased Item</label>

                  <div class="col-sm-8">
                      <select id="slct_items" class="form-control select2" multiple="multiple" style="width: 100%;">
                        <option>Caserole</option>
                        <option>Chopper</option>
                        <option>California</option>
                        <option>Delaware</option>
                        <option>Tennessee</option>
                        <option>Texas</option>
                        <option>Washington</option>
                      </select>
                  </div>
                  <label for="txt_client" class="control-label"><a class="hide-filter" style="cursor:pointer;"><i class="fa fa-times"></i></a></label>
                  
                </div>
                <!-- / Purchased Item -->

                <!-- Purchased Set -->
                <div class="form-group" id="set">
                  <label for="slct_set" class="col-sm-3 control-label">Purchased Set</label>

                  <div class="col-sm-8">
                      <select id="slct_set" class="form-control select2" multiple="multiple" style="width: 100%;">
                        <option>Alabama</option>
                        <option>Alaska</option>
                        <option>California</option>
                        <option>Delaware</option>
                        <option>Tennessee</option>
                        <option>Texas</option>
                        <option>Washington</option>
                      </select>
                  </div>
                  <label for="txt_client" class="control-label"><a class="hide-filter" style="cursor:pointer;"><i class="fa fa-times"></i></a></label>
                  
                </div>
                <!-- / Purchased Set -->

              </div>
              <!-- /.box-body -->
            </form>
          </div>
          <div class="col-md-2" style="margin-left:-30px">
            <form class="form-horizontal">
              <div class="box-body">
              <!-- Single button -->
                <div class="btn-group">
                  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Add Filter <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu">
                    <li class="date"><a href="#">Date Range</a><input type="hidden" value="date"></li>
                    <li class="client"><a href="#">Client</a><input type="hidden" value="client"></li>
                    <li class="po_no"><a href="#">PO Number</a><input type="hidden" value="po_no"></li>
                    <li class="item"><a href="#">Purchased Item</a><input type="hidden" value="item"></li>
                    <li class="set"><a href="#">Purchased Set</a><input type="hidden" value="set"></li>
                  </ul>
                </div>  
                <!-- / Single Button -->
              </div>
              <!-- /.box-body -->
            </form>
            <!-- /.form-horizontal -->   
          </div>
          <!-- /.box -->
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary"><i class="ion ion-search"></i> Search Purchase Order</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div id="add-role" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-blue">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="fa fa-key"></i> Add New User Role</h4>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-md-12">
            <!-- form start -->
            <form class="form-horizontal">
              <div class="box-body">

                <form class="form-horizontal">
                  <!-- Account Balance -->
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="txt_role_name" class="col-md-2 control-label"> Role Name</label>
                      <div class="col-md-10">
                        <input type="text" class="form-control pull-right" value="McIntire" id="txt_role_name">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="txt_role_desc" class="col-md-2 control-label"> Role Description</label>
                      <div class="col-md-10">
                        <textarea id="txt_role_desc"></textarea>
                      </div>

                    </div>
                  </div>
                </form>

              </div>
              <!-- /.box-body -->
            </form>
          </div>

        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary" id="btn_save_role"><i class="fa fa-save"></i> Save</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div id="delete-user" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-red-gradient">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="ion ion-search"></i> Deactivate User</h4>
      </div>
      <div class="modal-body">
        Do you want to deactivate this user?
        <input type="hidden" id="userId">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" id="btn_deactivate" class="btn btn-warning"><i class="ion ion-search"></i> Deactivate</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div id="activate-user" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-orange-active">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="ion ion-search"></i> Activate User</h4>
      </div>
      <div class="modal-body">
        Do you want to activate this user?
        <input type="hidden" id="userId">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" id="btn_activate" class="btn btn-warning" ><i class="ion ion-search"></i> Activate</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<div id="delete-agent" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-red-gradient">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="ion ion-search"></i> Deactivate Agent</h4>
      </div>
      <div class="modal-body">
        Do you want to deactivate this Agent?
        <input type="text" id="agentId">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" id="btn_deactivate_agent" class="btn btn-warning"><i class="ion ion-search"></i> Deactivate</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div id="activate-agent" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-orange-active">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="ion ion-search"></i> Activate Agent</h4>
      </div>
      <div class="modal-body">
        Do you want to activate this Agent?
        <input type="text" id="agentId">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" id="btn_activate_agent" class="btn btn-warning"><i class="ion ion-search"></i> Activate</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<div id="delete-client" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-red-gradient">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="ion ion-search"></i> Deactivate Client</h4>
      </div>
      <div class="modal-body">
        Do you want to deactivate this Agent?
        <input type="text" id="clientId">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" id="btn_deactivate_client" class="btn btn-warning"><i class="ion ion-search"></i> Deactivate</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div id="activate-client" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-orange-active">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="ion ion-search"></i> Activate Client</h4>
      </div>
      <div class="modal-body">
        Do you want to activate this Agent?
        <input type="text" id="clientId">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" id="btn_activate_client" class="btn btn-warning"><i class="ion ion-search"></i> Activate</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->



<div id="delete-product" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-red-gradient">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="ion ion-search"></i> Deactivate Product</h4>
      </div>
      <div class="modal-body">
        Do you want to deactivate this Product?
        <input type="text" id="productId">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" id="btn_deactivate_product" class="btn btn-warning"><i class="ion ion-search"></i> Deactivate</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div id="activate-product" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-orange-active">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="ion ion-search"></i> Activate Product</h4>
      </div>
      <div class="modal-body">
        Do you want to activate this Product?
        <input type="text" id="productId">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" id="btn_activate_product" class="btn btn-warning"><i class="ion ion-search"></i> Activate</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div id="view-booking" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-orange-active">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><i class="ion ion-search"></i> Booking Details</h4>
      </div>
      <div class="modal-body">
        <ul class="list-group list-group-unbordered">
          <li class="list-group-item">
            <b>Event Name</b> <a class="pull-right" id="booking_event_name">1,322</a>
          </li>
          <li class="list-group-item">
            <b>Client Name</b> <a class="pull-right" id="booking_client_name">543</a>
          </li>
          <li class="list-group-item">
            <b>Representative</b> <a class="pull-right" id="booking_representative_name">13,287</a>
          </li>
          <li class="list-group-item">
            <b>Address</b> <a class="pull-right" id="booking_address">13,287</a>
          </li>
        </ul>
        <input type="text" id="bookingId">
        <input type="text" id="eventId">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" id="btn_cancel_update" class="btn btn-warning"><i class="ion ion-close-round"></i> Cancel Booking</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
