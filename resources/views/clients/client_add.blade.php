@extends('admin_template')

@section('additional_header')

    <!-- DataTables -->
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>

@endsection

@section('content')
    <!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title"><i class="fa fa-user"></i> Personal Information</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">

            <div class="col-md-12">
              <form class="form-horizontal">
                <!-- Account Balance -->
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="txt_last_name" class="col-sm-2 control-label"> Last Name</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control pull-right" id="txt_last_name">
                    </div>
                    <label for="txt_first_name" class="col-sm-2 control-label"> First Name</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control pull-right" id="txt_first_name">
                    </div>

                  </div>
                </div>


                <div class="col-md-6">
                  <div class="form-group">
                    <label for="rdo_gender" class="col-sm-4 control-label"> Gender</label>

                    <div class="col-sm-8">
                      <label style="margin-top:2%;margin-left:2%">
                        <input type="radio" name="rdo_gender" value="Male" class="minimal" checked>
                        Male
                      </label>
                      <label style="margin-top:2%;margin-left:2%">
                        <input type="radio" name="rdo_gender" value="Female" class="minimal">
                        Female
                      </label>
                    </div>
                  </div>
                </div>


                <div class="col-md-6">
                  <div class="form-group">
                    <label for="txt_birthdate" class="col-sm-4 control-label birthdate"> BirthDate</label>

                    <div class="col-sm-8">

                      <input type="text" class="form-control pull-right" id="txt_birthdate">

                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="slct_civil_status" class="col-sm-4 control-label"> Civil Status</label>

                    <div class="col-sm-8">
                      <select id="slct_civil_status" class="form-control select2"
                              style="width: 100%;height:100%;background-color:white">
                        <option>Single</option>
                        <option>Married</option>
                        <option>Widow</option>
                        <option>Separated</option>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="txt_occupation" class="col-sm-4 control-label"> Occupation</label>

                    <div class="col-sm-8">

                      <input type="text" class="form-control pull-right" id="txt_occupation">

                    </div>
                  </div>
                </div>

                <div class="col-md-12">
                  <hr style="margin-bottom:1%">
                </div>

                <div class="col-md-12">
                  <h4 class="box-title"><i class="fa fa-phone"></i> Contact Information</h4>
                </div>

                <div class="col-md-12">
                  <div class="form-group">
                    <label for="txt_street" class="col-sm-2 control-label"> Street</label>

                    <div class="col-sm-10">

                      <input type="text" class="form-control pull-right" id="txt_street">

                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="txt_brgy" class="col-sm-4 control-label"> Barangay</label>

                    <div class="col-sm-8">
                      <input type="text" class="form-control pull-right" id="txt_brgy">
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="slct_town" class="col-sm-4 control-label"> Municipality</label>

                    <div class="col-sm-8">
                      <select id="slct_town" class="form-control select2"
                              style="width: 100%;height:100%;background-color:white">
                        <option value="">Select Municipality</option>
                        @foreach($municipalities as $municipality)
                          <option value="{{$municipality->citycode}}">{{$municipality->cityname}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="slct_province" class="col-sm-4 control-label"> Province</label>

                    <div class="col-sm-8">
                      <select id="slct_province" class="form-control select2"
                              style="width: 100%;height:100%;background-color:white">
                        <option value="">Select Province</option>
                        @foreach($provinces as $province)
                          <option value="{{$province->provcode}}">
                            {{$province->provname}}
                          </option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="txt_landline" class="col-sm-4 control-label"> Landline</label>

                    <div class="col-sm-8">
                      <input type="text" class="form-control pull-right" id="txt_landline">
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="txt_mobile" class="col-sm-4 control-label"> Mobile</label>

                    <div class="col-sm-8">
                      <input type="text" class="form-control pull-right" id="txt_mobile">
                    </div>
                  </div>
                </div>


                <div class="col-md-6">
                  <div class="form-group">
                    <label for="txt_email" class="col-sm-4 control-label"> Email</label>

                    <div class="col-sm-8">
                      <input type="text" class="form-control pull-right" id="txt_email">
                    </div>
                  </div>
                </div>

                <div class="col-md-12">
                  <hr style="margin-bottom:1%">
                </div>

                <div class="col-md-12">
                  <h4 class="box-title"><i class="fa fa-briefcase"></i> Business Information</h4>
                </div>

                <div class="col-md-12">
                  <div class="form-group">
                    <label for="txt_business_name" class="col-sm-2 control-label"> Business Name</label>

                    <div class="col-sm-10">

                      <input type="text" class="form-control pull-right" id="txt_business_name">

                    </div>
                  </div>
                </div>

                <div class="col-md-12">
                  <div class="form-group">
                    <label for="txt_business_street" class="col-sm-2 control-label"> Street</label>

                    <div class="col-sm-10">

                      <input type="text" class="form-control pull-right" id="txt_business_street">

                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="slct_business_brgy" class="col-sm-4 control-label"> Barangay</label>

                    <div class="col-sm-8">
                      <input type="text" class="form-control pull-right" id="slct_business_brgy">
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="slct_business_town" class="col-sm-4 control-label"> Municipality</label>

                    <div class="col-sm-8">
                      <select id="slct_business_town" class="form-control select2"
                              style="width: 100%;height:100%;background-color:white">
                        <option value="">Select Municipality</option>
                        @foreach($municipalities as $municipality)
                          <option value="{{$municipality->citycode}}">{{$municipality->cityname}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="slct_business_province" class="col-sm-4 control-label"> Province</label>

                    <div class="col-sm-8">
                      <select id="slct_business_province" class="form-control select2"
                              style="width: 100%;height:100%;background-color:white">
                        <option value="">Select Province</option>
                        @foreach($provinces as $province)
                          <option value="{{$province->provcode}}">
                            {{$province->provname}}
                          </option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="txt_business_landline" class="col-sm-4 control-label"> Landline</label>

                    <div class="col-sm-8">
                      <input type="text" class="form-control pull-right" id="txt_business_landline">
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="txt_business_mobile" class="col-sm-4 control-label"> Mobile</label>

                    <div class="col-sm-8">
                      <input type="text" class="form-control pull-right"
                             id="txt_business_mobile">
                    </div>
                  </div>
                </div>


                <div class="col-md-6">
                  <div class="form-group">
                    <label for="txt_business_email" class="col-sm-4 control-label"> Email</label>

                    <div class="col-sm-8">
                      <input type="text" class="form-control pull-right"
                             id="txt_business_email">
                    </div>
                  </div>
                </div>

                <div class="col-md-12">
                  <hr style="margin-bottom:1%">
                </div>

                <div class="col-md-12">
                  <h4 class="box-title"><i class="fa fa-check"></i> Valid ID</h4>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <img src="{{ asset("/bower_components/AdminLTE/dist/img/user2-160x160.jpg") }}"
                           alt="User profile picture" style="repeat-x:repeat">
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="txt_id_type" class="col-sm-3 control-label"> ID Type</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control pull-right"
                             style="background-color:white" id="txt_id_type">
                    </div>
                  </div>

                  <div class="form-group">
                    <label for="txt_id_no" class="col-sm-3 control-label"> ID No</label>

                    <div class="col-sm-9">
                      <input type="text" class="form-control pull-right" id="txt_id_no">
                    </div>
                  </div>


                </div>

                <div class="col-md-12">
                  <hr style="margin-bottom:1%">
                </div>

                <div class="col-md-12">
                  <h4 class="box-title"><i class="fa fa-pencil"></i> Signature Specimen</h4>
                </div>

                <div class="col-md-4">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <img src="{{ asset("/bower_components/AdminLTE/dist/img/user2-160x160.jpg") }}"
                           alt="User profile picture" style="repeat-x:repeat">
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <img src="{{ asset("/bower_components/AdminLTE/dist/img/user2-160x160.jpg") }}"
                           alt="User profile picture" style="repeat-x:repeat">
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group">
                    <div class="col-sm-12">
                      <img src="{{ asset("/bower_components/AdminLTE/dist/img/user2-160x160.jpg") }}"
                           alt="User profile picture" style="repeat-x:repeat">
                    </div>
                  </div>
                </div>

                <div class="col-md-12">
                  <hr style="margin-bottom:1%">
                </div>

              </form>

              <!-- buttons  -->
              <div class="col-md-4 col-md-offset-8">
                <div class="row">
                  <div class="col-md-12">
                    <button class="btn btn-info pull-right" id="btn_save_client"> Save Information</button>
                  </div>
                </div>
                <!-- /btn-group -->
              </div>
              <!-- /.buttons  -->

            </div><!-- col-md-12 -->
          </div><!-- /row -->
        </div>
        <!-- /.box-body -->
      </div>
    </div>
    <!-- /.col -->
  </div>
  <input type="text" id="prov_code">
  <input type="text" id="prov_code1">
</section>

@endsection

@section('additional_footer')

  <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
  <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>
  <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
  <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>

  <script>
    $(document).ready(function () {

      $(".select2").select2();
      $(".birthdate").daterangepicker({
        singleDatePicker: true,
        showDropdowns: true
      });

      $("#btn_save_client").click(function () {
        $.post("/clients/addNew", {
          txt_last_name: $("#txt_last_name").val(),
          txt_first_name: $("#txt_first_name").val(),
          rdo_gender: $("input[name=rdo_gender]:checked").val(),
          txt_birthdate: $("#txt_birthdate").val(),
          slct_civil_status: $("#slct_civil_status").val(),
          txt_occupation: $("#txt_occupation").val(),
          txt_street: $("#txt_street").val(),
          txt_brgy: $("#txt_brgy").val(),
          slct_town: $("#slct_town").val(),
          slct_province: $("#slct_province").val(),
          txt_landline: $("#txt_landline").val(),
          txt_mobile: $("#txt_mobile").val(),
          txt_email: $("#txt_email").val(),
          txt_business_street: $("#txt_business_street").val(),
          txt_business_name: $("#txt_business_name").val(),
          slct_business_brgy: $("#slct_business_brgy").val(),
          slct_business_town: $("#slct_business_town").val(),
          slct_business_province: $("#slct_business_province").val(),
          txt_business_landline: $("#txt_business_landline").val(),
          txt_business_mobile: $("#txt_business_mobile").val(),
          txt_business_email: $("#txt_business_email").val(),
          txt_id_type: $("#txt_id_type").val(),
          txt_id_no: $("#txt_id_no").val(),
          txt_occupation: $("#txt_occupation").val()
        }).done(function (data) {
          // document.write(data);
          window.location = '/clients?save=success';
        });
      });
      $("#slct_town").change(function () {
        $("#prov_code").val('0');
        if ($("#slct_town").val() == "") {
          $("#slct_province").select2('val', '');
          $.post("/municipalities", {
            provcode: ''
          }).done(function (data) {
            var list = "<option value=''>Display All Municipalities</option>";
            $("#slct_town").empty();
            for (var j = 0; j < data.length; j++) {
              list += "<option value='" + $("#slct_town").val() + "'>" + data[j].cityname + "</option>";
            }
            $("#slct_town").html(list);
          });
        }
        else {
          var province = $("#slct_town").val().substr(0, 4);
          $("#slct_province").select2('val', province);

        }
      });

      $("#slct_province").change(function () {
        console.log('b');
        if ($("#prov_code").val() != '0') {
          $.post("/municipalities", {
            provcode: $("#slct_province").val()
          }).done(function (data) {
            var list = "<option value=''>Display All Municipalities</option>";
            $("#slct_town").empty();
            for (var j = 0; j < data.length; j++) {
              list += "<option value='" + $("#slct_province").val() + "'>" + data[j].cityname + "</option>";
            }
            $("#slct_town").html(list);
            $("#slct_town").select2('val', $("#slct_province").val());
          });
        } else {
          $("#prov_code1").val('1');
        }
      });

      $("#slct_business_town").change(function () {
        $("#prov_code1").val('0');
        if ($("#slct_business_town").val() == "") {
          $("#slct_business_province").select2('val', '');
          $.post("/municipalities", {
            provcode: ''
          }).done(function (data) {
            var list = "<option value=''>Display All Municipalities</option>";
            $("#slct_business_town").empty();
            for (var j = 0; j < data.length; j++) {
              list += "<option value='" + $("#slct_town").val() + "'>" + data[j].cityname + "</option>";
            }
            $("#slct_business_town").html(list);
          });
        }
        else {
          var province = $("#slct_business_town").val().substr(0, 4);
          $("#slct_business_province").select2('val', province);

        }
      });

      $("#slct_business_province").change(function () {
        console.log('b');
        if ($("#prov_code1").val() != '0') {
          $.post("/municipalities", {
            provcode: $("#slct_business_province").val()
          }).done(function (data) {
            var list = "<option value=''>Display All Municipalities</option>";
            $("#slct_business_town").empty();
            for (var j = 0; j < data.length; j++) {
              list += "<option value='" + $("#slct_business_province").val() + "'>" + data[j].cityname + "</option>";
            }
            $("#slct_business_town").html(list);
            $("#slct_business_town").select2('val', $("#slct_business_province").val());
          });
        } else {
          $("#prov_code1").val('1');
        }
      });
    });</script>

@endsection