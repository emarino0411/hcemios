@extends('admin_template')

@section('additional_header')

    <!-- DataTables -->
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>

@endsection

@section('content')
    <!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title"><i class="fa fa-user"></i> Personal Information</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">

            <div class="col-md-12">

              <form class="form-horizontal">
                <!-- Account Balance -->
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="txt_last_name" class="col-sm-2 control-label"> Last Name</label>

                    <div class="col-sm-4">
                      <input type="text" style="background-color:white" class="form-control pull-right"
                             id="txt_last_name">
                    </div>
                    <label for="txt_first_name" class="col-sm-2 control-label"> First Name</label>

                    <div class="col-sm-4">
                      <input type="text" style="background-color:white" class="form-control pull-right"
                             id="txt_first_name">
                    </div>

                  </div>
                </div>


                <div class="col-md-6">
                  <div class="form-group">
                    <label for="rdo_gender" class="col-sm-4 control-label"> Gender</label>

                    <div class="col-sm-8">
                      <label style="margin-top:2%;margin-left:2%">
                        <input type="radio" name="rdo_gender" value="Male" class="minimal" checked>
                        Male
                      </label>
                      <label style="margin-top:2%;margin-left:2%">
                        <input type="radio" name="rdo_gender" value="Female" class="minimal">
                        Female
                      </label>
                    </div>
                  </div>
                </div>


                <div class="col-md-6">
                  <div class="form-group">
                    <label for="txt_agent_birthdate" class="col-sm-4 control-label"> BirthDate</label>

                    <div class="col-sm-8">

                      <input type="text" class="form-control pull-right birthdate"
                             style="background-color:white" id="txt_agent_birthdate">

                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="slct_civil_status" class="col-sm-4 control-label"> Civil Status</label>

                    <div class="col-sm-8">
                      <select id="slct_civil_status" class="form-control select2"
                              style="width: 100%;height:100%;background-color:white">
                        <option>Single</option>
                        <option>Married</option>
                        <option>Widdowed</option>
                        <option>Separated</option>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="slct_dependents" class="col-sm-4 control-label"> Dependents</label>

                    <div class="col-sm-8">
                      <select id="slct_dependents" class="form-control select2"
                              style="width: 100%;height:100%;background-color:white">
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="can_drive" class="col-sm-4 control-label"> Can Drive</label>

                    <div class="col-sm-8">
                      <label style="margin-top:2%;margin-left:2%">
                        <input type="radio" name="rdo_can_drive" value="Yes" class="minimal" checked>
                        Yes
                      </label>
                      <label style="margin-top:2%;margin-left:2%">
                        <input type="radio" name="rdo_can_drive" value="No" class="minimal">
                        No
                      </label>
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="own_car" class="col-sm-4 control-label"> Owns a Car</label>

                    <div class="col-sm-8">
                      <label style="margin-top:2%;margin-left:2%">
                        <input type="radio" name="rdo_own_car" value="Yes" class="minimal" checked>
                        Yes
                      </label>
                      <label style="margin-top:2%;margin-left:2%">
                        <input type="radio" name="rdo_own_car" value="No" class="minimal">
                        No
                      </label>
                    </div>
                  </div>
                </div>

                <div class="col-md-12">
                  <hr style="margin-bottom:1%">
                </div>

                <div class="col-md-12">
                  <h4 class="box-title"><i class="fa fa-user"></i> Spouse Information</h4>
                </div>

                <div class="col-md-12">
                  <div class="form-group">
                    <label for="txt_spouse_last_name" class="col-sm-2 control-label"> Last Name</label>

                    <div class="col-sm-4">
                      <input type="text" style="background-color:white" class="form-control pull-right"
                             id="txt_spouse_last_name">
                    </div>
                    <label for="txt_spouse_first_name" class="col-sm-2 control-label"> First Name</label>

                    <div class="col-sm-4">
                      <input type="text" style="background-color:white" class="form-control pull-right"
                             id="txt_spouse_first_name">
                    </div>

                  </div>
                </div>


                <div class="col-md-12">
                  <hr style="margin-bottom:1%">
                </div>

                <div class="col-md-12">
                  <h4 class="box-title"><i class="fa fa-phone"></i> Contact Information</h4>
                </div>

                <div class="col-md-12">
                  <div class="form-group">
                    <label for="txt_street" class="col-sm-2 control-label"> Street</label>

                    <div class="col-sm-10">

                      <input type="text" class="form-control pull-right" style="background-color:white"
                             id="txt_street">

                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="txt_brgy" class="col-sm-4 control-label"> Barangay</label>

                    <div class="col-sm-8">
                      <input type="text" class="form-control pull-right" style="background-color:white"
                             id="txt_brgy">
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="slct_town" class="col-sm-4 control-label"> Municipality</label>

                    <div class="col-sm-8">
                      <select id="slct_town" class="form-control select2"
                              style="width: 100%;height:100%;background-color:white">
                        <option value="">Select Municipality</option>
                        @foreach($municipalities as $municipality)
                          <option value="{{$municipality->citycode}}">{{$municipality->cityname}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="slct_province" class="col-sm-4 control-label"> Province</label>

                    <div class="col-sm-8">
                      <select id="slct_province" class="form-control select2"
                              style="width: 100%;height:100%;background-color:white">
                        <option value="">Select Province</option>
                        @foreach($provinces as $province)
                          <option value="{{$province->provcode}}">
                            {{$province->provname}}
                          </option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="txt_agent_landline" class="col-sm-4 control-label"> Landline</label>

                    <div class="col-sm-8">
                      <input type="text" class="form-control pull-right"
                             style="background-color:white" id="txt_agent_landline">
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="txt_agent_mobile" class="col-sm-4 control-label"> Mobile</label>

                    <div class="col-sm-8">
                      <input type="text" class="form-control pull-right"
                             style="background-color:white" id="txt_agent_mobile">
                    </div>
                  </div>
                </div>


                <div class="col-md-6">
                  <div class="form-group">
                    <label for="txt_agent_email" class="col-sm-4 control-label"> Email</label>

                    <div class="col-sm-8">
                      <input type="text" class="form-control pull-right"
                             style="background-color:white" id="txt_agent_email">
                    </div>
                  </div>
                </div>

                <div class="col-md-12">
                  <hr style="margin-bottom:1%">
                </div>

                <div class="col-md-12">
                  <h4 class="box-title"><i class="fa fa-user"></i> Sponsor Information</h4>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="txt_agent_sponsor" class="col-sm-4 control-label"> Sponsor</label>

                    <div class="col-sm-8">
                      <div class="input-group">
                        <input type="text" class="form-control pull-right"
                               style="background-color:white" id="txt_agent_sponsor">

                        <div class="input-group-btn">
                          <button type="button" class="btn btn-info"><i class="fa fa-search"></i></button>
                        </div>
                        <!-- /btn-group -->
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="txt_agent_sponsor_date" class="col-sm-4 control-label "> Sponsor Date</label>

                    <div class="col-sm-8">

                      <input type="text" class="form-control pull-right birthdate"
                             style="background-color:white" id="txt_agent_sponsor_date">

                    </div>
                  </div>
                </div>


                <div class="col-md-12">
                  <hr style="margin-bottom:1%">
                </div>

                <div class="col-md-12">
                  <h4 class="box-title"><i class="fa fa-credit-card"></i> Government Contributions ID</h4>
                </div>

                <div class="col-md-12">
                  <div class="form-group">
                    <label for="txt_sss" class="col-sm-2 control-label"> SSS</label>

                    <div class="col-sm-4">
                      <input type="text" style="background-color:white" class="form-control pull-right"
                             id="txt_sss">
                    </div>
                    <label for="txt_tin" class="col-sm-2 control-label"> TIN ID</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control pull-right" id="txt_tin">
                    </div>

                  </div>
                </div>

                <div class="col-md-12">
                  <div class="form-group">
                    <label for="txt_pagibig" class="col-sm-2 control-label"> PAGIBIG NO</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control pull-right" id="txt_pagibig">
                    </div>
                    <label for="txt_philhealth" class="col-sm-2 control-label"> PHILHEALTH NO</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control pull-right" id="txt_philhealth">
                    </div>

                  </div>
                </div>

                <div class="col-md-12">
                  <hr style="margin-bottom:1%">
                </div>

                <div class="col-md-12">
                  <h4 class="box-title"><i class="fa fa-building"></i> Previous Employer</h4>
                </div>

                <div class="col-md-12">
                  <div class="form-group">
                    <label for="txt_employer_name" class="col-sm-2 control-label"> Name</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control pull-right" id="txt_employer_name">
                    </div>
                    <label for="txt_employer_address" class="col-sm-2 control-label"> Address</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control pull-right" id="txt_employer_address">
                    </div>

                  </div>
                </div>

                <div class="col-md-12">
                  <div class="form-group">
                    <label for="txt_employer_contact" class="col-sm-2 control-label"> Contact No</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control pull-right" id="txt_employer_contact">
                    </div>
                    <label for="txt_employer_email" class="col-sm-2 control-label"> Email</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control pull-right" id="txt_employer_email">
                    </div>

                  </div>
                </div>

              </form>

              <!-- buttons  -->
              <div class="col-md-4 col-md-offset-8">
                <div class="row">
                  <div class="col-md-12">
                    <button class="btn btn-info pull-right" id="btn_save_agent"> Save Information</button>
                  </div>
                </div>
                <!-- /btn-group -->
              </div>
              <!-- /.buttons  -->

            </div><!-- col-md-12 -->
          </div><!-- /row -->
        </div>
        <!-- /.box-body -->
      </div>
    </div>
    <!-- /.col -->
  </div>

  <input type="hidden" id="prov_code">
</section>

@endsection

@section('additional_footer')

  <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
  <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>
  <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
  <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>


  <script>
    $(document).ready(function () {
      $(".select2").select2();
      $(".birthdate").daterangepicker({
        singleDatePicker: true,
        showDropdowns: true
      });

      $("#btn_save_agent").click(function () {
        $.post("/agents/addNew", {
          txt_first_name: $("#txt_first_name").val(),
          txt_last_name: $("#txt_last_name").val(),
          rdo_gender: $("input[name=rdo_gender]:checked").val(),
          txt_agent_birthdate: $("#txt_agent_birthdate").val(),
          slct_civil_status: $("#slct_civil_status").val(),
          slct_dependents: $("#slct_dependents").val(),
          rdo_can_drive: $("input[name=rdo_can_drive]:checked").val(),
          rdo_own_car: $("input[name=rdo_own_car]:checked").val(),
          txt_spouse_last_name: $("#txt_spouse_last_name").val(),
          txt_spouse_first_name: $("#txt_spouse_first_name").val(),
          txt_street: $("#txt_street").val(),
          txt_brgy: $("#txt_brgy").val(),
          slct_town: $("#slct_town").val(),
          slct_province: $("#slct_province").val(),
          txt_agent_landline: $("#txt_agent_landline").val(),
          txt_agent_mobile: $("#txt_agent_mobile").val(),
          txt_agent_email: $("#txt_agent_email").val(),
          txt_agent_sponsor: $("#txt_agent_sponsor").val(),
          txt_agent_sponsor_date: $("#txt_agent_sponsor_date").val(),
          txt_sss: $("#txt_sss").val(),
          txt_tin: $("#txt_tin").val(),
          txt_pagibig: $("#txt_pagibig").val(),
          txt_philhealth: $("#txt_philhealth").val(),
          txt_employer_name: $("#txt_employer_name").val(),
          txt_employer_address: $("#txt_employer_address").val(),
          txt_employer_contact: $("#txt_employer_contact").val(),
          txt_employer_email: $("#txt_employer_email").val()
        }).done(function (data) {
          // document.write(data);
          window.location = '/agents?save=success';
        });
      });

      $("#slct_town").change(function () {
        $("#prov_code").val('0');
        if ($("#slct_town").val() == "") {
          $("#slct_province").select2('val','');
          $.post("/municipalities", {
            provcode: ''
          }).done(function (data) {
            var list = "<option value=''>Display All Municipalities</option>";
            $("#slct_town").empty();
            for (var j = 0; j < data.length; j++) {
              list += "<option value='" + $("#slct_town").val() + "'>" + data[j].cityname + "</option>";
            }
            $("#slct_town").html(list);
          });
        }
        else {
          var province = $("#slct_town").val().substr(0,4);
          $("#slct_province").select2('val',province);

        }
      });

      $("#slct_province").change(function () {
        console.log('b');
        if ($("#prov_code").val() != '0') {
          $.post("/municipalities", {
            provcode: $("#slct_province").val()
          }).done(function (data) {
            var list = "<option value=''>Display All Municipalities</option>";
            $("#slct_town").empty();
            for (var j = 0; j < data.length; j++) {
              list += "<option value='" + $("#slct_province").val() + "'>" + data[j].cityname + "</option>";
            }
            $("#slct_town").html(list);
            $("#slct_town").select2('val', $("#slct_province").val());
          });
        }else{
          $("#prov_code").val('1');
        }
      });
    })
  </script>


@endsection