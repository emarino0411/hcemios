@extends('admin_template')

@section('additional_header')

    <!-- DataTables -->
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>

@endsection

@section('content')
    <!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-3">

      <!-- Profile Image -->
      <div class="box box-primary">
        <div class="box-body box-profile">
          <img class="profile-user-img img-responsive img-circle"
               src="{{ asset("/bower_components/AdminLTE/dist/img/user2-160x160.jpg") }}" alt="User profile picture">

          <h3 class="profile-username text-center">{{ucwords($agent->first_name)}} {{ucwords($agent->last_name)}}</h3>

          <p class="text-muted text-center">Agent</p>
          <hr>

          <p class="text-muted text-center"><b>Agent Code</b><br>{{$agent->representative_code}}</p>
          <hr>
          <p class="text-muted text-center"><b>Address</b><br>{{$agent->street}} {{$agent->brgy}}, {{$agent->city}}
            , {{$agent->province}}</p>
          <hr>
          <p class="text-muted text-center"><b>Contact Information</b><br>{{$agent->phone1}}/{{$agent->phone2}}</p>
          <hr>
          <p class="text-muted text-center"><b>User Status</b><br>{{$agent->status}}</p>
          <hr>
          <center>
            <button class="btn btn-primary" id="btn_update">Update Information</button>
          </center>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </div>
    <!-- /.col -->
    <div class="col-md-9">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#profile" data-toggle="tab">Agent Information</a></li>
          <li><a href="#sales" data-toggle="tab">Sales</a></li>
          <li><a href="#wsp" data-toggle="tab">WSP</a></li>
          <li><a href="#account_balance" data-toggle="tab">Account Balance</a></li>
          <li><a href="#group" data-toggle="tab">Group</a></li>
        </ul>
        <div class="tab-content">
          <div class="active tab-pane" id="profile">

            <div class="row">
              <div class="col-md-12">
                <div class="">
                  <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-user"></i> Personal Information</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div class="row">

                      <div class="col-md-12">

                        <form class="form-horizontal">
                          <!-- Account Balance -->
                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="txt_last_name" class="col-sm-2 control-label"> Last Name</label>

                              <div class="col-sm-4">
                                <input type="text" style="background-color:white" class="form-control pull-right"
                                       value="{{$agent->last_name}}" id="txt_last_name">
                                <input type="hidden" style="background-color:white" class="form-control pull-right"
                                       value="{{$agent->id}}" id="txt_agent_id">
                              </div>
                              <label for="txt_first_name" class="col-sm-2 control-label"> First Name</label>

                              <div class="col-sm-4">
                                <input type="text" style="background-color:white" class="form-control pull-right"
                                       value="{{$agent->first_name}}" id="txt_first_name">
                              </div>

                            </div>
                          </div>


                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="rdo_gender" class="col-sm-4 control-label"> Gender</label>

                              <div class="col-sm-8">
                                <label style="margin-top:2%;margin-left:2%">
                                  <input type="radio" name="rdo_gender" value="Male"
                                         class="minimal" @if($agent->gender=='Male') {{'checked'}} @endif>
                                  Male
                                </label>
                                <label style="margin-top:2%;margin-left:2%">
                                  <input type="radio" name="rdo_gender" value="Female"
                                         class="minimal" @if($agent->gender=='Female') {{'checked'}} @endif>
                                  Female
                                </label>
                              </div>
                            </div>
                          </div>


                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="txt_agent_birthdate" class="col-sm-4 control-label"> BirthDate</label>

                              <div class="col-sm-8">

                                <input type="text" class="form-control pull-right birthdate"
                                       value="{{$agent->birthdate}}" style="background-color:white"
                                       id="txt_agent_birthdate">

                              </div>
                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="slct_civil_status" class="col-sm-4 control-label"> Civil Status</label>

                              <div class="col-sm-8">
                                <select id="slct_civil_status" class="form-control select2"
                                        style="width: 100%;height:100%;background-color:white">
                                  <option @if($agent->civil_status=='Single') selected @endif>Single</option>
                                  <option @if($agent->civil_status=='Married') selected @endif>Married</option>
                                  <option @if($agent->civil_status=='Widowed') selected @endif>Widowed</option>
                                  <option @if($agent->civil_status=='Separated') selected @endif>Separated</option>
                                </select>
                              </div>
                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="slct_dependents" class="col-sm-4 control-label"> Dependents</label>

                              <div class="col-sm-8">
                                <select id="slct_dependents" class="form-control select2"
                                        style="width: 100%;height:100%;background-color:white">
                                  <option @if($agent->no_of_dependents=='0') selected @endif value="0">0</option>
                                  <option @if($agent->no_of_dependents=='1') selected @endif value="1">1</option>
                                  <option @if($agent->no_of_dependents=='2') selected @endif value="2">2</option>
                                  <option @if($agent->no_of_dependents=='3') selected @endif value="3">3</option>
                                  <option @if($agent->no_of_dependents=='4') selected @endif value="4">4</option>
                                  <option @if($agent->no_of_dependents=='5') selected @endif value="5">5</option>
                                  <option @if($agent->no_of_dependents=='6') selected @endif value="6">6</option>
                                  <option @if($agent->no_of_dependents=='7') selected @endif value="7">7</option>
                                  <option @if($agent->no_of_dependents=='8') selected @endif value="8">8</option>
                                  <option @if($agent->no_of_dependents=='9') selected @endif value="9">9</option>
                                  <option @if($agent->no_of_dependents=='10') selected @endif value="10">10</option>
                                </select>
                              </div>
                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="can_drive" class="col-sm-4 control-label"> Can Drive</label>

                              <div class="col-sm-8">
                                <label style="margin-top:2%;margin-left:2%">
                                  <input type="radio"
                                         @if($agent->can_drive=='Yes') {{'checked'}} @endif name="rdo_can_drive"
                                         value="Yes" class="minimal" checked>
                                  Yes
                                </label>
                                <label style="margin-top:2%;margin-left:2%">
                                  <input type="radio"
                                         @if($agent->can_drive=='No') {{'checked'}} @endif name="rdo_can_drive"
                                         value="No" class="minimal">
                                  No
                                </label>
                              </div>
                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="own_car" class="col-sm-4 control-label"> Owns a Car</label>

                              <div class="col-sm-8">
                                <label style="margin-top:2%;margin-left:2%">
                                  <input type="radio" @if($agent->own_car=='No') {{'checked'}} @endif name="rdo_own_car"
                                         value="Yes" class="minimal" checked>
                                  Yes
                                </label>
                                <label style="margin-top:2%;margin-left:2%">
                                  <input type="radio" @if($agent->own_car=='No') {{'checked'}} @endif name="rdo_own_car"
                                         value="No" class="minimal">
                                  No
                                </label>
                              </div>
                            </div>
                          </div>

                          <div class="col-md-12">
                            <hr style="margin-bottom:1%">
                          </div>

                          <div class="col-md-12">
                            <h4 class="box-title"><i class="fa fa-user"></i> Spouse Information</h4>
                          </div>

                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="txt_spouse_last_name" class="col-sm-2 control-label"> Last Name</label>

                              <div class="col-sm-4">
                                <input type="text" style="background-color:white" class="form-control pull-right"
                                       value="{{$agent->spouse_last_name}}" id="txt_spouse_last_name">
                              </div>
                              <label for="txt_spouse_first_name" class="col-sm-2 control-label"> First Name</label>

                              <div class="col-sm-4">
                                <input type="text" style="background-color:white" class="form-control pull-right"
                                       value="{{$agent->spouse_first_name}}" id="txt_spouse_first_name">
                              </div>

                            </div>
                          </div>


                          <div class="col-md-12">
                            <hr style="margin-bottom:1%">
                          </div>

                          <div class="col-md-12">
                            <h4 class="box-title"><i class="fa fa-phone"></i> Contact Information</h4>
                          </div>

                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="txt_street" class="col-sm-2 control-label"> Street</label>

                              <div class="col-sm-10">

                                <input type="text" class="form-control pull-right" style="background-color:white"
                                       value="{{$agent->street}}" id="txt_street">

                              </div>
                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="txt_brgy" class="col-sm-4 control-label"> Barangay</label>

                              <div class="col-sm-8">
                                <input type="text" class="form-control pull-right" style="background-color:white"
                                       value="{{$agent->brgy}}" id="txt_brgy">
                              </div>
                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="slct_town" class="col-sm-4 control-label"> Municipality</label>

                              <div class="col-sm-8">
                                <select id="slct_town" class="form-control select2"
                                        style="width: 100%;height:100%;background-color:white">
                                  @foreach($municipalities as $municipality)
                                    <option
                                        value="{{$municipality->citycode}}" @if($agent->city == $municipality->citycode){{'selected'}} @endif>{{$municipality->cityname}}</option>
                                  @endforeach
                                </select>
                              </div>
                            </div>
                          </div>


                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="slct_province" class="col-sm-4 control-label"> Province</label>

                              <div class="col-sm-8">
                                <select id="slct_province" class="form-control select2"
                                        style="width: 100%;height:100%;background-color:white">
                                  @foreach($provinces as $province)
                                    <option
                                        value="{{$province->provcode}}" @if($agent->province == $province->provcode){{'selected'}} @endif>
                                      {{$province->provname}}
                                    </option>
                                  @endforeach
                                </select>
                              </div>
                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="txt_agent_landline" class="col-sm-4 control-label"> Landline</label>

                              <div class="col-sm-8">
                                <input type="text" class="form-control pull-right"
                                       value="{{$agent->phone1}}" style="background-color:white"
                                       id="txt_agent_landline">
                              </div>
                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="txt_agent_mobile" class="col-sm-4 control-label"> Mobile</label>

                              <div class="col-sm-8">
                                <input type="text" class="form-control pull-right"
                                       value="{{$agent->phone2}}" style="background-color:white" id="txt_agent_mobile">
                              </div>
                            </div>
                          </div>


                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="txt_agent_email" class="col-sm-4 control-label"> Email</label>

                              <div class="col-sm-8">
                                <input type="text" class="form-control pull-right"
                                       value="{{$agent->email_address}}" style=" background-color:white"
                                       id="txt_agent_email">
                              </div>
                            </div>
                          </div>

                          <div class="col-md-12">
                            <hr style="margin-bottom:1%">
                          </div>

                          <div class="col-md-12">
                            <h4 class="box-title"><i class="fa fa-user"></i> Sponsor Information</h4>
                          </div>

                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="txt_agent_sponsor" class="col-sm-4 control-label"> Sponsor</label>

                              <div class="col-sm-8">
                                <div class="input-group">
                                  <input type="text" class="form-control pull-right"
                                         value="{{$agent->sponsor_id}}" style=" background-color:white"
                                         id="txt_agent_sponsor">

                                  <div class="input-group-btn">
                                    <button type="button" class="btn btn-info"><i class="fa fa-search"></i></button>
                                  </div>
                                  <!-- /btn-group -->
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="txt_agent_sponsor_date" class="col-sm-4 control-label "> Sponsor Date</label>

                              <div class="col-sm-8">

                                <input type="text" class="form-control pull-right birthdate"
                                       value="{{$agent->sponsor_date}}" style="background-color:white"
                                       id="txt_agent_sponsor_date">

                              </div>
                            </div>
                          </div>


                          <div class="col-md-12">
                            <hr style="margin-bottom:1%">
                          </div>

                          <div class="col-md-12">
                            <h4 class="box-title"><i class="fa fa-credit-card"></i> Government Contributions ID</h4>
                          </div>

                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="txt_sss" class="col-sm-2 control-label"> SSS</label>

                              <div class="col-sm-4">
                                <input type="text" style="background-color:white" class="form-control pull-right"
                                       value="{{$agent->sss_no}}" id="txt_sss">
                              </div>
                              <label for="txt_tin" class="col-sm-2 control-label"> TIN ID</label>

                              <div class="col-sm-4">
                                <input type="text" class="form-control pull-right" value="{{$agent->tin_no}}"
                                       id="txt_tin">
                              </div>

                            </div>
                          </div>

                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="txt_pagibig" class="col-sm-2 control-label"> PAGIBIG NO</label>

                              <div class="col-sm-4">
                                <input type="text" class="form-control pull-right" value="{{$agent->pagibig_no}}"
                                       id="txt_pagibig">
                              </div>
                              <label for="txt_philhealth" class="col-sm-2 control-label"> PHILHEALTH NO</label>

                              <div class="col-sm-4">
                                <input type="text" class="form-control pull-right" value="{{$agent->philhealth_no}}"
                                       id="txt_philhealth">
                              </div>

                            </div>
                          </div>

                          <div class="col-md-12">
                            <hr style="margin-bottom:1%">
                          </div>

                          <div class="col-md-12">
                            <h4 class="box-title"><i class="fa fa-building"></i> Previous Employer</h4>
                          </div>

                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="txt_employer_name" class="col-sm-2 control-label"> Name</label>

                              <div class="col-sm-4">
                                <input type="text" class="form-control pull-right" value="{{$agent->previous_employer}}"
                                       id="txt_employer_name">
                              </div>
                              <label for="txt_employer_address" class="col-sm-2 control-label"> Address</label>

                              <div class="col-sm-4">
                                <input type="text" class="form-control pull-right" value="{{$agent->p_employer_addr}}"
                                       id="txt_employer_address">
                              </div>

                            </div>
                          </div>

                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="txt_employer_contact" class="col-sm-2 control-label"> Contact No</label>

                              <div class="col-sm-4">
                                <input type="text" class="form-control pull-right" value="{{$agent->p_employer_telno}}"
                                       id="txt_employer_contact">
                              </div>
                              <label for="txt_employer_email" class="col-sm-2 control-label"> Email</label>

                              <div class="col-sm-4">
                                <input type="text" class="form-control pull-right" value="{{$agent->p_employer_email}}"
                                       id="txt_employer_email">
                              </div>

                            </div>
                          </div>

                        </form>

                        <!-- buttons  -->
                        <div class="col-md-3 col-md-offset-9" id="div_update_user">
                          <div class="row">
                            <div class="col-md-12">
                              <button type="button" id="btn_save_agent" class="btn btn-info pull-right"> Update</button>
                              <button class="btn btn-warning" id="btn_cancel_update"> Cancel</button>
                            </div>
                          </div>
                          <!-- /btn-group -->
                        </div>
                        <!-- /.buttons  -->

                      </div><!-- col-md-12 -->
                    </div><!-- /row -->
                  </div>
                  <!-- /.box-body -->
                </div>
              </div>
              <!-- /.col -->
            </div>

            <input type="hidden" id="prov_code">

          </div>
          <!-- /.tab-pane -->
          <div class="tab-pane" id="timeline">
            <!-- The timeline -->
            <ul class="timeline timeline-inverse">
              <!-- timeline time label -->
              <li class="time-label">
                        <span class="bg-red">
                          10 Feb. 2014
                        </span>
              </li>
              <!-- /.timeline-label -->
              <!-- timeline item -->
              <li>
                <i class="fa fa-envelope bg-blue"></i>

                <div class="timeline-item">
                  <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>

                  <h3 class="timeline-header"><a href="#">Support Team</a> sent you an email</h3>

                  <div class="timeline-body">
                    Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                    weebly ning heekya handango imeem plugg dopplr jibjab, movity
                    jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                    quora plaxo ideeli hulu weebly balihoo...
                  </div>
                  <div class="timeline-footer">
                    <a class="btn btn-primary btn-xs">Read more</a>
                    <a class="btn btn-danger btn-xs">Delete</a>
                  </div>
                </div>
              </li>
              <!-- END timeline item -->
              <!-- timeline item -->
              <li>
                <i class="fa fa-user bg-aqua"></i>

                <div class="timeline-item">
                  <span class="time"><i class="fa fa-clock-o"></i> 5 mins ago</span>

                  <h3 class="timeline-header no-border"><a href="#">Sarah Young</a> accepted your friend request
                  </h3>
                </div>
              </li>
              <!-- END timeline item -->
              <!-- timeline item -->
              <li>
                <i class="fa fa-comments bg-yellow"></i>

                <div class="timeline-item">
                  <span class="time"><i class="fa fa-clock-o"></i> 27 mins ago</span>

                  <h3 class="timeline-header"><a href="#">Jay White</a> commented on your post</h3>

                  <div class="timeline-body">
                    Take me to your leader!
                    Switzerland is small and neutral!
                    We are more like Germany, ambitious and misunderstood!
                  </div>
                  <div class="timeline-footer">
                    <a class="btn btn-warning btn-flat btn-xs">View comment</a>
                  </div>
                </div>
              </li>
              <!-- END timeline item -->
              <!-- timeline time label -->
              <li class="time-label">
                        <span class="bg-green">
                          3 Jan. 2014
                        </span>
              </li>
              <!-- /.timeline-label -->
              <!-- timeline item -->
              <li>
                <i class="fa fa-camera bg-purple"></i>

                <div class="timeline-item">
                  <span class="time"><i class="fa fa-clock-o"></i> 2 days ago</span>

                  <h3 class="timeline-header"><a href="#">Mina Lee</a> uploaded new photos</h3>

                  <div class="timeline-body">
                    <img src="http://placehold.it/150x100" alt="..." class="margin">
                    <img src="http://placehold.it/150x100" alt="..." class="margin">
                    <img src="http://placehold.it/150x100" alt="..." class="margin">
                    <img src="http://placehold.it/150x100" alt="..." class="margin">
                  </div>
                </div>
              </li>
              <!-- END timeline item -->
              <li>
                <i class="fa fa-clock-o bg-gray"></i>
              </li>
            </ul>
          </div>
          <!-- /.tab-pane -->

          <div class="tab-pane" id="settings">
            <form class="form-horizontal">
              <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Name</label>

                <div class="col-sm-10">
                  <input type="email" class="form-control" id="inputName" placeholder="Name">
                </div>
              </div>
              <div class="form-group">
                <label for="inputEmail" class="col-sm-2 control-label">Email</label>

                <div class="col-sm-10">
                  <input type="email" class="form-control" id="inputEmail" placeholder="Email">
                </div>
              </div>
              <div class="form-group">
                <label for="inputName" class="col-sm-2 control-label">Name</label>

                <div class="col-sm-10">
                  <input type="text" class="form-control" id="inputName" placeholder="Name">
                </div>
              </div>
              <div class="form-group">
                <label for="inputExperience" class="col-sm-2 control-label">Experience</label>

                <div class="col-sm-10">
                  <textarea class="form-control" id="inputExperience" placeholder="Experience"></textarea>
                </div>
              </div>
              <div class="form-group">
                <label for="inputSkills" class="col-sm-2 control-label">Skills</label>

                <div class="col-sm-10">
                  <input type="text" class="form-control" id="inputSkills" placeholder="Skills">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox"> I agree to the <a href="#">terms and conditions</a>
                    </label>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-danger">Submit</button>
                </div>
              </div>
            </form>
          </div>
          <!-- /.tab-pane -->
        </div>
        <!-- /.tab-content -->
      </div>
      <!-- /.nav-tabs-custom -->

    </div>
    <!-- /.col -->
  </div>
</section>

@endsection

@section('additional_footer')

  <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
  <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>
  <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
  <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>


  <script>
    $(document).ready(function () {
      $(".select2").select2();
      $(".birthdate").daterangepicker({
        singleDatePicker: true,
        showDropdowns: true
      });
        @if(!$edit_mode){
        cancelEdit();
      }
      @endif

      $("#btn_update").click(function () {
        console.log('hahahah');
        $("#txt_first_name").removeAttr('disabled');
        $("#txt_last_name").removeAttr('disabled');
        $("input[name=rdo_gender]:checked").removeAttr('disabled');
        $("#txt_agent_birthdate").removeAttr('disabled');
        $("#slct_civil_status").removeAttr('disabled');
        $("#slct_dependents").removeAttr('disabled');
        $("input[name=rdo_can_drive]").removeAttr('disabled');
        $("input[name=rdo_own_car]").removeAttr('disabled');
        $("#txt_spouse_last_name").removeAttr('disabled');
        $("#txt_spouse_first_name").removeAttr('disabled');
        $("#txt_street").removeAttr('disabled');
        $("#txt_brgy").removeAttr('disabled');
        $("#slct_town").removeAttr('disabled');
        $("#slct_province").removeAttr('disabled');
        $("#txt_agent_landline").removeAttr('disabled');
        $("#txt_agent_mobile").removeAttr('disabled');
        $("#txt_agent_email").removeAttr('disabled');
        $("#txt_agent_sponsor").removeAttr('disabled');
        $("#txt_agent_sponsor_date").removeAttr('disabled');
        $("#txt_sss").removeAttr('disabled');
        $("#txt_tin").removeAttr('disabled');
        $("#txt_pagibig").removeAttr('disabled');
        $("#txt_philhealth").removeAttr('disabled');
        $("#txt_employer_name").removeAttr('disabled');
        $("#txt_employer_address").removeAttr('disabled');
        $("#txt_employer_contact").removeAttr('disabled');
        $("#txt_employer_email").removeAttr('disabled');
        $("#div_update_user").show();

      });

      $("#btn_cancel_update").click(function () {
        cancelEdit();
      });

      function cancelEdit() {
        $("#txt_user_last_name").attr('disabled', 'true');
        $("#txt_first_name").attr('disabled', 'true');
        $("#txt_last_name").attr('disabled', 'true');
        $("input[name=rdo_gender]").attr('disabled', 'true');
        $("#txt_agent_birthdate").attr('disabled', 'true');
        $("#slct_civil_status").attr('disabled', 'true');
        $("#slct_dependents").attr('disabled', 'true');
        $("input[name=rdo_can_drive]").attr('disabled', 'true');
        $("input[name=rdo_own_car]").attr('disabled', 'true');
        $("#txt_spouse_last_name").attr('disabled', 'true');
        $("#txt_spouse_first_name").attr('disabled', 'true');
        $("#txt_street").attr('disabled', 'true');
        $("#txt_brgy").attr('disabled', 'true');
        $("#slct_town").attr('disabled', 'true');
        $("#slct_province").attr('disabled', 'true');
        $("#txt_agent_landline").attr('disabled', 'true');
        $("#txt_agent_mobile").attr('disabled', 'true');
        $("#txt_agent_email").attr('disabled', 'true');
        $("#txt_agent_sponsor").attr('disabled', 'true');
        $("#txt_agent_sponsor_date").attr('disabled', 'true');
        $("#txt_sss").attr('disabled', 'true');
        $("#txt_tin").attr('disabled', 'true');
        $("#txt_pagibig").attr('disabled', 'true');
        $("#txt_philhealth").attr('disabled', 'true');
        $("#txt_employer_name").attr('disabled', 'true');
        $("#txt_employer_address").attr('disabled', 'true');
        $("#txt_employer_contact").attr('disabled', 'true');
        $("#txt_employer_email").attr('disabled', 'true');
        $("#div_update_user").hide();

        $("#txt_user_last_name").css('background-color', 'white');
      }


      $("#btn_save_agent").click(function () {
        $.post("/agents/update", {
          txt_first_name: $("#txt_first_name").val(),
          txt_last_name: $("#txt_last_name").val(),
          rdo_gender: $("input[name=rdo_gender]:checked").val(),
          txt_agent_birthdate: $("#txt_agent_birthdate").val(),
          slct_civil_status: $("#slct_civil_status").val(),
          slct_dependents: $("#slct_dependents").val(),
          rdo_can_drive: $("input[name=rdo_can_drive]:checked").val(),
          rdo_own_car: $("input[name=rdo_own_car]:checked").val(),
          txt_spouse_last_name: $("#txt_spouse_last_name").val(),
          txt_spouse_first_name: $("#txt_spouse_first_name").val(),
          txt_street: $("#txt_street").val(),
          txt_brgy: $("#txt_brgy").val(),
          slct_town: $("#slct_town").val(),
          slct_province: $("#slct_province").val(),
          txt_agent_landline: $("#txt_agent_landline").val(),
          txt_agent_mobile: $("#txt_agent_mobile").val(),
          txt_agent_email: $("#txt_agent_email").val(),
          txt_agent_sponsor: $("#txt_agent_sponsor").val(),
          txt_agent_sponsor_date: $("#txt_agent_sponsor_date").val(),
          txt_sss: $("#txt_sss").val(),
          txt_tin: $("#txt_tin").val(),
          txt_pagibig: $("#txt_pagibig").val(),
          txt_philhealth: $("#txt_philhealth").val(),
          txt_employer_name: $("#txt_employer_name").val(),
          txt_employer_address: $("#txt_employer_address").val(),
          txt_employer_contact: $("#txt_employer_contact").val(),
          txt_employer_email: $("#txt_employer_email").val(),
          txt_agent_id: $("#txt_agent_id").val()
        }).done(function (data) {
          window.location = '/agents?update=success';
        });
      });

      $("#slct_town").change(function () {
        console.log('a');
        if ($("#slct_town").val() == "") {
          $("#prov_code").val('0');
          $.post("/municipalities", {
            provcode: ''
          }).done(function (data) {
            var list = "<option value=''>Display All Municipalities</option>";
            $("#slct_town").empty();
            for (var j = 0; j < data.length; j++) {
              list += "<option value='" + $("#slct_town").val() + "'>" + data[j].cityname + "</option>";
            }
            $("#slct_town").html(list);
          });
        } else {
          $.post("/provinces", {
                citycode: $("#slct_town").val()
              })
              .done(function (data) {
                $("#prov_code").val(data);
                $("#slct_province").select2('val', data);

              });
        }

      });

      $("#slct_province").change(function () {
        console.log('b');
        if ($("#prov_code").val() != $("#slct_province").val()) {
          $.post("/municipalities", {
            provcode: $("#slct_province").val()
          }).done(function (data) {
            var list = "<option value=''>Display All Municipalities</option>";
            $("#slct_town").empty();
            for (var j = 0; j < data.length; j++) {
              list += "<option value='" + $("#slct_province").val() + "'>" + data[j].cityname + "</option>";
            }
            $("#slct_town").html(list);
            $("#slct_town").select2('val', $("#slct_province").val());
          });
        }
      });
    })
  </script>
@endsection