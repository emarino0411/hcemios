@extends('admin_template')

@section('additional_header')

    <!-- DataTables -->
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>

@endsection

@section('content')
    <!-- Main content -->
<section class="content">

  <div class="row">
    <div class="col-md-3">

      <!-- Profile Image -->
      <div class="box box-primary">
        <div class="box-body box-profile">
          <img class="profile-user-img img-responsive img-circle"
               src="{{ asset("/bower_components/AdminLTE/dist/img/user2-160x160.jpg") }}" alt="User profile picture">

          <h3 class="profile-username text-center">{{$user->first_name . ' ' .$user->last_name}}</h3>

          <p class="text-muted text-center">{{$user->user_role}}</p>
          <hr>
          <p class="text-muted text-center"><b>Address</b><br>{{$user->street}} {{$user->brgy}}, {{$user->city}}
            , {{$user->province}}</p>
          <hr>
          <p class="text-muted text-center"><b>Contact Information</b><br>{{$user->mobile}}/{{$user->landline}}</p>
          <hr>
          <p class="text-muted text-center"><b>User Status</b><br>Active</p>
          <hr>
          <center>
            <button class="btn btn-primary" id="btn_update">Update Information</button>
          </center>

        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->

    </div>
    <!-- /.col -->
    <div class="col-md-9">
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li class="active"><a href="#profile" data-toggle="tab">User Information</a></li>
          <li><a href="#sales" data-toggle="tab">User Logs</a></li>
        </ul>
        <div class="tab-content">
          <div class="active tab-pane" id="profile">

            <div class="row">
              <div class="col-md-12">
                <div class="">
                  <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-user"></i> Personal Information</h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <div class="row">

                      <div class="col-md-12">
                        <form class="form-horizontal">
                          <!-- Account Balance -->
                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="txt_user_last_name" class="col-sm-2 control-label"> Last Name</label>

                              <div class="col-sm-4">
                                <input type="text" class="form-control pull-right" value="{{$user->last_name}}"
                                       id="txt_user_last_name">
                              </div>
                              <label for="txt_user_first_name" class="col-sm-2 control-label"> First Name</label>

                              <div class="col-sm-4">
                                <input type="text" class="form-control pull-right" value="{{$user->first_name}}"
                                       id="txt_user_first_name">
                              </div>

                            </div>
                          </div>


                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="rdo_gender" class="col-sm-4 control-label"> Gender</label>

                              <div class="col-sm-8">
                                <label style="margin-top:2%;margin-left:2%">
                                  <input type="radio" name="rdo_gender" value="Male"
                                         class="minimal" @if($user->gender=='Male') {{'checked'}} @endif>
                                  Male
                                </label>
                                <label style="margin-top:2%;margin-left:2%">
                                  <input type="radio" name="rdo_gender" value="Female"
                                         class="minimal" @if($user->gender=='Female') {{'checked'}} @endif>
                                  Female
                                </label>
                              </div>
                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="txt_user_birthdate" class="col-sm-4 control-label"> BirthDate</label>

                              <div class="col-sm-8">
                                <input type="text" class="form-control pull-right" value="{{$user->birthdate}}"
                                       id="txt_user_birthdate">
                              </div>
                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="slct_civil_status" class="col-sm-4 control-label"> Civil Status</label>

                              <div class="col-sm-8">
                                <select id="slct_civil_status" class="form-control select2"
                                        style="width: 100%;height:100%;background-color:white">
                                  <option @if($user->civil_status=='Single') selected @endif>Single</option>
                                  <option @if($user->civil_status=='Married') selected @endif>Married</option>
                                  <option @if($user->civil_status=='Widowed') selected @endif>Widowed</option>
                                  <option @if($user->civil_status=='Separated') selected @endif>Separated</option>
                                </select>
                              </div>
                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="slct_user_role" class="col-sm-4 control-label"> User Role</label>

                              <div class="col-sm-8">
                                <select id="slct_user_role" class="form-control select2"
                                        style="width: 100%;height:100%;background-color:white">
                                  @foreach($roles as $role)
                                    <option value="{{$role['id']}}">{{$role['name']}}</option>
                                  @endforeach
                                </select>
                              </div>
                            </div>
                          </div>


                          <div class="col-md-12">
                            <hr style="margin-bottom:1%">
                          </div>

                          <div class="col-md-12">
                            <h4 class="box-title"><i class="fa fa-phone"></i> Contact Information</h4>
                          </div>

                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="txt_user_street" class="col-sm-2 control-label"> Street</label>

                              <div class="col-sm-10">

                                <input type="text" class="form-control pull-right" value="{{$user->street}}"
                                       id="txt_user_street">

                              </div>
                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="slct_user_barangay" class="col-sm-4 control-label"> Barangay</label>

                              <div class="col-sm-8">
                                <input type="text" id="txt_brgy" class="form-control">
                              </div>
                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="slct_town" class="col-sm-4 control-label"> Municipality</label>

                              <div class="col-sm-8">
                                <select id="slct_town" class="form-control select2"
                                        style="width: 100%;height:100%;background-color:white">
                                  @foreach($municipalities as $municipality)
                                    <option
                                        value="{{$municipality->provname}}" @if($user->city == $municipality->cityname){{'selected'}} @endif>{{$municipality->cityname}}</option>
                                  @endforeach
                                </select>
                              </div>
                            </div>
                          </div>


                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="slct_province" class="col-sm-4 control-label"> Province</label>

                              <div class="col-sm-8">
                                <select id="slct_province" class="form-control select2"
                                        style="width: 100%;height:100%;background-color:white">
                                  @foreach($provinces as $province)
                                    <option @if($user->province == $province->provname){{'selected'}} @endif>
                                      {{$province->provname}}
                                    </option>
                                  @endforeach
                                </select>
                              </div>
                            </div>
                          </div>


                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="txt_user_landline" class="col-sm-4 control-label"> Landline</label>

                              <div class="col-sm-8">
                                <input type="text" class="form-control pull-right" value="{{$user->landline}}"
                                       id="txt_user_landline">
                              </div>
                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="txt_user_mobile" class="col-sm-4 control-label"> Mobile</label>

                              <div class="col-sm-8">
                                <input type="text" class="form-control pull-right" value="{{$user->mobile}}"
                                       id="txt_user_mobile">
                              </div>
                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="txt_user_email" class="col-sm-4 control-label"> Email</label>

                              <div class="col-sm-8">
                                <input type="text" class="form-control pull-right" value="{{$user->email}}"
                                       id="txt_user_email">
                                <input type="hidden" class="form-control pull-right" value="{{$user->email}}"
                                       id="hdn_user_email">
                                <input type="hidden" class="form-control pull-right" value="{{$user->id}}"
                                       id="hdn_user_id">
                              </div>
                            </div>
                          </div>

                          <div class="col-md-12">
                            <hr style="margin-bottom:1%">
                          </div>
                        </form>

                        <!-- buttons  -->
                        <div class="col-md-3 col-md-offset-9" id="div_update_user">
                          <div class="row">
                            <div class="col-md-12">
                              <button type="button" id="btn_save_user" class="btn btn-info pull-right"> Update</button>
                              <button class="btn btn-warning" id="btn_cancel_update"> Cancel</button>
                            </div>
                          </div>
                          <!-- /btn-group -->
                        </div>
                        <!-- /.buttons  -->

                      </div><!-- col-md-12 -->
                    </div><!-- /row -->
                  </div>
                  <!-- /.box-body -->
                </div>
              </div>
              <!-- /.col -->
            </div>

          </div>

        </div>
        <!-- /.tab-content -->
      </div>
      <!-- /.nav-tabs-custom -->

    </div>
    <!-- /.col -->
  </div>
</section>

@endsection

@section('additional_footer')

  <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
  <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>
  <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
  <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>


  <script>
    $(document).ready(function () {

      $('.select2').select2();
      $("#txt_user_birthdate").daterangepicker({
        singleDatePicker: true,
        showDropdowns: true
      });
      $('.select2').attr('height', '200%');
      @if(!$edit_mode){
        cancelEdit();
      }
      @endif


      $('#example1').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
      });

      $("#btn_search").click(function () {
        $(".select2").select2({
          theme: "classic"
        });
        $('#txt_daterange').daterangepicker();

      });

      $(".hide-filter").each(function (index) {
        $(this).parent().parent().hide();
        $(this).on("click", function () {
          $(this).parent().parent().hide();
          $("." + $(this).parent().parent().attr('id')).show();
        });
      });

      $(".dropdown-menu li").each(function (index) {
        $(this).on("click", function () {
          $("#" + $(this).children("input").val()).show();
          $(this).hide();
        });
      });

      $("#btn_update").click(function () {
        console.log('hahahah');
        $("#txt_user_last_name").removeAttr('disabled');
        $("#txt_user_first_name").removeAttr('disabled');
        $("rdo_gender").removeAttr('disabled');
        $("#txt_user_birthdate").removeAttr('disabled');
        $("#slct_civil_status").removeAttr('disabled');
        $("#slct_user_role").removeAttr('disabled');
        $("#txt_user_street").removeAttr('disabled');
        $("#slct_user_barangay").removeAttr('disabled');
        $("#slct_town").removeAttr('disabled');
        $("#slct_province").removeAttr('disabled');
        $("#txt_user_landline").removeAttr('disabled');
        $("#txt_user_mobile").removeAttr('disabled');
        $("#txt_user_email").removeAttr('disabled');
        $("#div_update_user").show();

      });

      $("#btn_cancel_update").click(function () {
        cancelEdit();
      });

      function cancelEdit() {
        $("#txt_user_last_name").attr('disabled', 'true');
        $("#txt_user_first_name").attr('disabled', 'true');
        $("rdo_gender").attr('disabled', 'true');
        $("#txt_user_birthdate").attr('disabled', 'true');
        $("#slct_civil_status").attr('disabled', 'true');
        $("#slct_user_role").attr('disabled', 'true');
        $("#txt_user_street").attr('disabled', 'true');
        $("#slct_user_barangay").attr('disabled', 'true');
        $("#slct_town").attr('disabled', 'true');
        $("#slct_province").attr('disabled', 'true');
        $("#txt_user_landline").attr('disabled', 'true');
        $("#txt_user_mobile").attr('disabled', 'true');
        $("#txt_user_email").attr('disabled', 'true');
        $("#div_update_user").hide();

        $("#txt_user_last_name").css('background-color', 'white');
        $("#txt_user_first_name").css('background-color', 'white');
        $("rdo_gender").css('background-color', 'white');
        $("#txt_user_birthdate").css('background-color', 'white');
        $("#slct_civil_status").css('background', 'white');
        $("#slct_user_role").css('background-color', 'white');
        $("#txt_user_street").css('background-color', 'white');
        $("#slct_user_barangay").css('background', 'white');
        $("#slct_town").css('background', 'white');
        $("#slct_province").css('background', 'white');
        $("#txt_user_landline").css('background-color', 'white');
        $("#txt_user_mobile").css('background-color', 'white');
        $("#txt_user_email").css('background-color', 'white');
        $('.select2').css('background-color', 'white');
      }

      $("#btn_save_user").click(function () {
        $.post("/users/update", {
              user_last_name: $("#txt_user_last_name").val(),
              user_first_name: $("#txt_user_first_name").val(),
              user_gender: $("input[name=rdo_gender]:checked").val(),
              user_birthdate: $("#txt_user_birthdate").val(),
              user_civil_status: $("#slct_civil_status").val(),
              user_role: $("#slct_user_role").val(),
              user_street: $("#txt_user_street").val(),
              user_brgy: $("#slct_user_barangay").val(),
              user_town: $("#slct_town").val(),
              user_province: $("#slct_province").val(),
              user_landline: $("#txt_user_landline").val(),
              user_mobile: $("#txt_user_mobile").val(),
              user_email: $("#txt_user_email").val(),
              user_password: $("#txt_user_password").val(),
              old_email: $("#hdn_user_email").val(),
              user_id: $("#hdn_user_id").val()
            }
        ).done(function (data) {
          if (data == 'USER_SAVED') {
            console.log('role updated');
          } else {

          }
        });
      });
    });
  </script>
@endsection