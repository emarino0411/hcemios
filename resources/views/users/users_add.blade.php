@extends('admin_template')

@section('additional_header')

    <!-- DataTables -->
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>

@endsection

@section('content')
    <!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title"><i class="fa fa-user"></i> Personal Information</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-12 hidden" id="div_error">
              <div class="alert alert-warning">
                <strong><i class="fa fa-warning"></i></strong> There were some problems with your input.
              </div>
            </div>

            <div class="col-md-12">
              <form class="form-horizontal">
                <!-- Account Balance -->
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="txt_user_last_name" class="col-sm-2 control-label">
                      Last Name</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control pull-right" id="txt_user_last_name" data-toggle="tooltip"
                             title="Alphanumeric characters only.">
                    </div>
                    <label for="txt_user_first_name" class="col-sm-2 control-label"> First
                      Name</label>

                    <div class="col-sm-4">
                      <input type="text" class="form-control pull-right" id="txt_user_first_name" data-toggle="tooltip"
                             title="Alphanumeric characters only.">
                    </div>

                  </div>
                </div>


                <div class="col-md-6">
                  <div class="form-group">
                    <label for="rdo_gender" class="col-sm-4 control-label"> Gender</label>

                    <div class="col-sm-8">
                      <label style="margin-top:2%;margin-left:2%">
                        <input type="radio" name="rdo_gender" value="Male" class="minimal"
                               checked>
                        Male
                      </label>
                      <label style="margin-top:2%;margin-left:2%">
                        <input type="radio" name="rdo_gender" value="Female" class="minimal">
                        Female
                      </label>
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="txt_user_birthdate" class="col-sm-4 control-label">
                      BirthDate</label>

                    <div class="col-sm-8">
                      <input type="text" class="form-control pull-right" id="txt_user_birthdate" data-toggle="tooltip"
                             title="Follow format mm/dd/yyyy">
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="slct_civil_status" class="col-sm-4 control-label"> Civil
                      Status</label>

                    <div class="col-sm-8">
                      <select id="slct_civil_status" class="form-control select2"
                              style="width: 100%;height:100%;background-color:white" data-toggle="tooltip"
                              title="Civil Status">
                        <option>Single</option>
                        <option>Married</option>
                        <option>Widowed</option>
                        <option>Separated</option>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="slct_user_role" class="col-sm-4 control-label"> User Role</label>

                    <div class="col-sm-8">
                      <select id="slct_user_role" class="form-control select2"
                              style="width: 100%;height:100%;background-color:white" data-toggle="tooltip"
                              title="System Access Rights Role">
                        @foreach($roles as $role)
                          <option value="{{$role['id']}}">{{$role['name']}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="txt_user_password" class="col-sm-4 control-label"> Password</label>

                    <div class="col-sm-8">
                      <input type="password" class="form-control pull-right" value="" id="txt_user_password"
                             data-toggle="tooltip" title="Minimum password length: 6">
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="txt_user_password2" class="col-sm-4 control-label"> Repeat Password</label>

                    <div class="col-sm-8">
                      <input type="password" class="form-control pull-right" value="" id="txt_user_password2"
                             data-toggle="tooltip" title="Minimum password length: 6">
                    </div>
                  </div>
                </div>

                <div class="col-md-12">
                  <hr style="margin-bottom:1%">
                </div>

                <div class="col-md-12">
                  <h4 class="box-title"><i class="fa fa-phone"></i> Contact Information</h4>
                </div>

                <div class="col-md-12">
                  <div class="form-group">
                    <label for="txt_user_street" class="col-sm-2 control-label"> Street</label>

                    <div class="col-sm-10">

                      <input type="text" class="form-control pull-right" id="txt_user_street" data-toggle="tooltip"
                             title="Alphanumeric characters only">

                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="txt_brgy" class="col-sm-4 control-label"> Barangay</label>

                    <div class="col-sm-8">
                      <input type="text" id="txt_brgy" class="form-control">
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="slct_town" class="col-sm-4 control-label"> Municipality</label>

                    <div class="col-sm-8">
                      <select id="slct_town" class="form-control select2"
                              style="width: 100%;height:100%;background-color:white">
                        <option value="">Select Municipality</option>
                        @foreach($municipalities as $municipality)
                          <option value="{{$municipality->provcode}}">{{$municipality->cityname}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>


                <div class="col-md-6">
                  <div class="form-group">
                    <label for="slct_province" class="col-sm-4 control-label"> Province</label>

                    <div class="col-sm-8">
                      <select id="slct_province" class="form-control select2"
                              style="width: 100%;height:100%;background-color:white">
                        <option value="">Select Province</option>
                        @foreach($provinces as $province)
                          <option value="{{$province->provcode}}">
                            {{$province->provname}}
                          </option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>


                <div class="col-md-6">
                  <div class="form-group">
                    <label for="txt_user_landline" class="col-sm-4 control-label"> Landline</label>

                    <div class="col-sm-8">
                      <input type="text" class="form-control pull-right" id="txt_user_landline">
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="txt_user_mobile" class="col-sm-4 control-label"> Mobile</label>

                    <div class="col-sm-8">
                      <input type="text" class="form-control pull-right" id="txt_user_mobile">
                    </div>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label for="txt_user_email" class="col-sm-4 control-label"> Email</label>

                    <div class="col-sm-8">
                      <input type="text" class="form-control pull-right" id="txt_user_email">
                    </div>
                  </div>
                </div>

                <div class="col-md-12">
                  <hr style="margin-bottom:1%">
                </div>
              </form>

              <!-- buttons  -->
              <div class="col-md-2 col-md-offset-10 ">
                <div class="row">
                  <div class="col-md-12">
                    <button type="button" id="btn_save_user" class="btn btn-info pull-right"> Save
                    </button>

                    <button class="btn btn-danger"> Cancel</button>
                  </div>
                </div>
                <!-- /btn-group -->
              </div>
              <!-- /.buttons  -->

            </div><!-- col-md-12 -->
          </div><!-- /row -->
        </div>
        <!-- /.box-body -->
      </div>
    </div>
    <!-- /.col -->
  </div>
  <input type="text" id="prov_code">
</section>

@endsection

@section('additional_footer')

  <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
  <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>
  <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
  <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>

  <script>
    $(document).ready(function () {
      $(".select2").select2();
      $("#txt_user_birthdate").daterangepicker({
        singleDatePicker: true,
        showDropdowns: true
      });

      $("#btn_save_user").click(function () {

        var result = $("#txt_user_password2").val().match($("#txt_user_password").val());

        if (result == null) {
          $("#txt_user_password").prev().addClass("has-error");
          $("#txt_user_password").prev().removeClass("hidden");
          $("#txt_user_password").parent().addClass("has-error");
          $("#txt_user_password2").parent().addClass("has-error");
          $("#div_error").removeClass('hidden');
          return;
        }
        $.post("/users/addUser", {
          user_last_name: $("#txt_user_last_name").val(),
          user_first_name: $("#txt_user_first_name").val(),
          user_gender: $("input[name=rdo_gender]:checked").val(),
          user_birthdate: $("#txt_user_birthdate").val(),
          user_civil_status: $("#slct_civil_status").val(),
          user_role: $("#slct_user_role").val(),
          user_street: $("#txt_user_street").val(),
          user_brgy: $("#txt_brgy").val(),
          user_town: $("#slct_town").val(),
          user_province: $("#slct_province").val(),
          user_landline: $("#txt_user_landline").val(),
          user_mobile: $("#txt_user_mobile").val(),
          user_email: $("#txt_user_email").val(),
          user_password: $("#txt_user_password").val()
        }).done(function (data) {
          window.location = '/users?save=success';
        });
      });

      $("#slct_town").change(function () {
        $("#prov_code").val('0');
        if ($("#slct_town").val() == "") {
          $("#slct_province").select2('val','');
          $.post("/municipalities", {
            provcode: ''
          }).done(function (data) {
            var list = "<option value=''>Display All Municipalities</option>";
            $("#slct_town").empty();
            for (var j = 0; j < data.length; j++) {
              list += "<option value='" + $("#slct_town").val() + "'>" + data[j].cityname + "</option>";
            }
            $("#slct_town").html(list);
          });
        }
        else {
          var province = $("#slct_town").val().substr(0,4);
          $("#slct_province").select2('val',province);

        }
      });

      $("#slct_province").change(function () {
        console.log('b');
        if ($("#prov_code").val() != '0') {
          $.post("/municipalities", {
            provcode: $("#slct_province").val()
          }).done(function (data) {
            var list = "<option value=''>Display All Municipalities</option>";
            $("#slct_town").empty();
            for (var j = 0; j < data.length; j++) {
              list += "<option value='" + $("#slct_province").val() + "'>" + data[j].cityname + "</option>";
            }
            $("#slct_town").html(list);
            $("#slct_town").select2('val', $("#slct_province").val());
          });
        }else{
          $("#prov_code").val('1');
        }
      });
    });
  </script>

@endsection