@extends('admin_template')

@section('additional_header')

    <!-- DataTables -->
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>

@endsection

@section('content')
    <!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12 col-xs-12">
      <!-- general form elements -->
      <div class="com-md-12 box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Search Product</h3>
        </div>
        <!-- /.box-header -->
        <!-- form start -->
        <div class="box-body">
          <div class="row">
            <form role="form">
              <div class="col-md-2 col-sm-2">
                <button id="btn_search" type="button" class="btn btn-primary" data-toggle="modal"
                        data-target="#search-purchase"><i class="ion ion-search"></i> Search Client Records
                </button>
              </div>
              <div class="col-md-2 col-sm-2 col-md-offset-5 col-sm-offset-5">
                <a href="/products/addNew" class="btn btn-success pull-right"><i class="fa fa-user"></i> Add New product</a>
              </div>
              <div class="col-md-3">
                <div class="input-group">
                  <select class="form-control">
                    <option>--Select Action--</option>
                    <option>Verified</option>
                    <option>Bounce Check</option>
                    <option>Error on Check</option>
                  </select>

                  <div class="input-group-btn">
                    <button type="button" class="btn btn-info">Go</button>
                  </div>
                  <!-- /btn-group -->
                </div>
              </div>

            </form>
          </div>
          <!-- /row -->

        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
      <!--/.col (left) -->
    </div>
    <!-- search form -->
  </div>
  <!-- /.row (main row) -->

  <div class="row">
    <div class=" col-md-12 col-xs-12">
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title"><i class="fa fa-shopping-cart"></i> Product Records</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>&nbsp;</th>
              <th>Product Code</th>
              <th>Product Name</th>
              <th>Retail Price</th>
              <th>Inventory Price</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($products as $product)
              <tr>
                <td><input type="checkbox"></td>
                <td>{{$product['item_code']}}</td>
                <td>{{$product['description']}}</td>
                <td>{{$product['retail_price']}}</td>
                <td>{{$product['inventory_price']}}</td>
                <td>{{$product['status']}}</td>
                <td>
                  <a href="products/viewProduct/{{$product['id']}}"
                     data-toggle="tooltip"
                     title="View {{$product['item_code']}}'s details"
                     class="btn btn-success btn-xs">
                    <i class="fa fa-info-circle"></i> View
                  </a>
                  <a href="products/editProfile/{{$product['id']}}/edit"
                     data-toggle="tooltip"
                     title="Edit {{$product['item_code']}}'s details"
                     class="btn btn-primary btn-xs">
                    <i class="fa fa-edit"></i> Edit
                  </a>
                  @if($product['status']=='Active')
                    <button type="button" class="btn btn-danger btn-xs deactivate-product" data-toggle="modal"
                            data-id="{{$product['id']}}"
                            data-target="#delete-product"
                            title="Deactivate {{$product['item_code']}}"
                    ><i class="fa fa-warning"></i> Deactivate
                    </button>
                  @else
                    <button type="button" class="btn btn-warning btn-xs activate-product" data-toggle="modal"
                            data-id="{{$product['id']}}"
                            data-target="#activate-product"><i class="fa fa-power-off"></i> Activate
                    </button>
                  @endif

                  {{--<a href="users/deleteProfile/{{$product['id']}}"--}}
                  {{--data-toggle="tooltip"--}}
                  {{--title="Deactivate {{$product['first_name']}} {{$product['last_name']}}'s details"--}}
                  {{--class="btn btn-warning btn-xs"--}}
                  {{--data-toggle="modal"--}}
                  {{--data-target="#search-purchase">--}}
                  {{--<i class="fa fa-warning"></i> Deactivate--}}
                  {{--</a>--}}
                </td>
              </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
              <th>&nbsp;</th>
              <th>Product Code</th>
              <th>Product Name</th>
              <th>Retail Price</th>
              <th>Inventory Price</th>
              <th>Action</th>
            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
  </div>
</section>

@endsection

@section('additional_footer')

  <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
  <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>
  <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
  <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>


  <script>
    $(document).ready(function () {
      $('#example1').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
      });

      $("#btn_search").click(function () {
        $(".select2").select2({
          theme: "classic"
        });
        $('#txt_daterange').daterangepicker();

      });

      $(".hide-filter").each(function (index) {
        $(this).parent().parent().hide();
        $(this).on("click", function () {
          $(this).parent().parent().hide();
          $("." + $(this).parent().parent().attr('id')).show();
        });
      });

      $(".dropdown-menu li").each(function (index) {
        $(this).on("click", function () {
          $("#" + $(this).children("input").val()).show();
          $(this).hide();
        });
      });

      $(document).on("click", ".deactivate-product", function () {
        var myBookId = $(this).data('id');

        $(".modal-body #productId").val(myBookId);
      });

      $("#btn_deactivate_product").click(function () {
        $.post("/products/delete", {
          productId: $(".modal-body #productId").val()
        }).done(function (data) {
          window.location = '/products?deactivate=success';
        });
      });

      $(document).on("click", ".activate-product", function () {
        var myBookId = $(this).data('id');
        var button = $(this);
        $(".modal-body #productId").val(myBookId);
      });

      $("#btn_activate_product").click(function () {
        $.post("/products/activate", {
          productId: $(".modal-body #productId").val()
        }).done(function (data) {
          window.location = '/products?reactivate=success';

        });
      });
    });
  </script>
@endsection