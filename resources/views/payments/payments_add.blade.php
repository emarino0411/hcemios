@extends('admin_template')

@section('additional_header')

<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/iCheck/all.css")}}'>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>

@endsection

@section('content')
    <!-- Main content -->
    <section class="content">

      <!-- Client Information -->
      <div class="row">
        <div class=" col-md-12 col-xs-12">
          <div class="row">
            <!-- client -->
            <div class="col-md-12">              
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title"><i class="fa fa-user"></i> Search Client</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-12">
                      <form class="form-horizontal">
                        <div class="col-md-5">
                          <div class="form-group">
                            <label for="txt_po_no" class="col-sm-4 control-label">Add Payment For</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                  <div class="input-group-addon">
                                    <i class="fa fa-user"></i>
                                  </div>
                                  <input type="text" class="form-control pull-right" id="txt_po_no">
                                </div>
                                <!-- /.input group -->
                            </div>
                          </div>
                        </div>
                        <div class="col-md-1 col-sm-1">
                          <button id="btn_search" type="button" class="btn btn-primary" data-toggle="modal" data-target="#search-purchase"><i class="ion ion-search"></i> Search Client </button>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="txt_po_no" class="col-sm-4 control-label">Select PO No</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                  <div class="input-group-addon">
                                    <i class="fa fa-edit"></i>
                                  </div>
                                  <select class="form-control">
                                    <option>PO-1545</option>
                                    <option>PO-1546</option>
                                    <option>PO-1547</option>
                                  </select>
                                </div>
                                <!-- /.input group -->
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
                <!-- /.box-body -->
              </div>              
            </div>
            <!-- /client -->

          </div>
        </div>
      </div>
      <!-- / client information --> 

      <!-- Payment Details -->
      <div class="row">
        <div class="col-md-12 col-xs-12">

          <div class="com-md-12 box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><i class="fa fa-money"></i> Payment Details</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal">
              <div class="box-body">

                <div class="row">
                  <div class="col-md-12">

                    <!-- Payment Date -->
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="txt_payment_date" class="col-sm-3 control-label">Payment Date</label>
                        <div class="col-sm-9">
                            <div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                              <input type="text" class="form-control pull-right" id="txt_payment_date">
                            </div>
                            
                            <!-- /.input group -->
                        </div>
                        <!-- /.col-sm-8 -->
                      </div>
                      <!-- /.form-group -->
                    </div>
                    <!-- /Payment Date -->


                    <!-- Payment Type -->
                    <div class="col-md-6">
                      <div class="form-group">
                        <label for="slct_payment_type" id="lbl_item_set" class="col-sm-3 control-label">Payment Type</label>
                        <div class="col-sm-9">                           
                          <select id="slct_payment_type" class="set form-control select2" style="width: 100%;height:100%;">
                            <option value="cash">Cash</option>
                            <option value="check">Check</option>
                          </select>       
                        </div>
                        <!-- /.col-sm-9 -->
                      </div>
                      <!-- /.form-group -->
                    </div>
                    <!-- Payment Type -->

                    <div class="col-md-12">
                    <hr style="margin:1%"> 
                    </div>
                    

                    <!-- Cash -->  
                 
                    <div class="col-md-6 col-md-offset-6" id="div_cash">

                       <div class="form-group">
                          <label for="txt_amount_cash" class="col-sm-3 control-label">Cash Amount</label>
                          <div class="col-sm-9">
                            <div class="input-group">
                              <div class="input-group-addon">
                                <i class="fa fa-rub"></i>
                              </div>
                              <input type="text" class="form-control pull-right" id="txt_amount_cash">
                            </div>
                            
                            <!-- /.input group -->
                        </div>
                        <!-- /.col-sm-9 -->
                        </div>
                      <!-- /. form-group -->
                    </div> 

                    <!-- //Cash --> 


                    <!-- Check -->
                    <div class="row" id="div_check" style="display:none;">
                      <!-- form start -->
                      <form class="form-horizontal">
                        <div class="box-body">
                          <div class="row">
                            <div class="col-md-12">

                              <div class="col-md-10 col-md-offset-1">
                                <h6 class="page-header"> <i class="fa fa-list-alt"></i> Check Details</h6> 
                              </div>
                              <!-- Bank-->
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="slct_bank" class="col-sm-3 control-label">Bank</label>
                                  <div class="col-sm-9">
                                      <div class="input-group">
                                        <div class="input-group-addon">
                                          <i class="fa fa-bank"></i>
                                        </div>
                                        <select id="slct_bank" class="form-control select2" style="width: 100%;height:100%">
                                          <option>BPI</option>
                                          <option>BDO</option>                                            
                                          <option>Landbank</option>
                                          <option>Metrobank</option>
                                          <option>PSBank</option>
                                        </select>
                                      </div>
                                      <!-- /.input group -->
                                  </div>
                                </div>
                              </div>
                              <!-- /Bank -->

                              <!-- Branch-->
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="txt_branch" class="col-sm-3 control-label">Branch</label>
                                  <div class="col-sm-9">
                                      <div class="input-group">
                                        <div class="input-group-addon">
                                          <i class="fa fa-map-marker"></i>
                                        </div>                                          
                                        <input type="text" class="form-control pull-right" id="txt_branch">
                                      </div>
                                      <!-- /.input group -->
                                  </div>
                                </div>
                              </div>
                              <!-- /Branch -->
                              
                              <!-- Check Date -->
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="txt_check_date" class="col-sm-3 control-label">Check Date</label>
                                  <div class="col-sm-9">
                                      <div class="input-group">
                                        <div class="input-group-addon">
                                          <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right" id="txt_check_date">
                                      </div>
                                      
                                      <!-- /.input group -->
                                  </div>
                                  <!-- /.col-sm-9 -->
                                </div>
                                <!-- /.form-group -->
                              </div>
                              <!-- /Check Date --> 
                              
                              <!-- Check No-->
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="txt_check_no" class="col-sm-3 control-label">Check No</label>
                                  <div class="col-sm-9">
                                      <div class="input-group">
                                        <div class="input-group-addon">
                                          <i class="fa fa-edit"></i>
                                        </div>                                          
                                        <input type="text" class="form-control pull-right" id="txt_check_no">
                                      </div>
                                      <!-- /.input group -->
                                  </div>
                                </div>
                              </div>
                              <!-- /Check No -->
                              
                             <!-- Check No-->
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label for="txt_check_amount" class="col-sm-3 control-label">Check Amount</label>
                                  <div class="col-sm-9">
                                      <div class="input-group">
                                        <div class="input-group-addon">
                                          <i class="fa fa-rub"></i>
                                        </div>                                          
                                        <input type="text" class="form-control pull-right" id="txt_check_amount">
                                      </div>
                                      <!-- /.input group -->
                                  </div>
                                </div>
                              </div>
                              <!-- /Check No -->


                            </div>
                            <!-- ./col-md-12 -->
                          </div>
                          <!-- /.row-->
                        </div>
                        <!-- /.box-body -->
                      </form>
                      <!-- /form-end -->   
                    </div>               
                    <!-- //Check --> 

                     <!-- Add to List -->
                    <div class="col-md-6 col-md-offset-6">
                      <button class="btn btn-primary pull-right"><i class="fa fa-plus"></i>Add to List</button> 
                      
                    </div>
                    <!-- /Add to List -->

                  </div>
                  <!-- ./col-md-12 --> 
                </div>
                <!-- /.row-->

                <!-- Purchase Item Table -->
                <div class="row">
                  <div class="col-md-12">
                    <h6 class="page-header"> <i class="fa fa-list-alt"></i> Payments List</h6> 
                    <div class="row">
                      <form role="form">
                        <div class="col-md-2 col-sm-2">
                          <button id="btn_search" type="button" class="btn btn-primary" data-toggle="modal" data-target="#search-purchase"><i class="ion ion-search"></i> Search Payment Records </button>
                        </div>
                        <div class="col-md-3 col-md-offset-7">
                         <div class="input-group">
                            <select class="form-control">
                              <option>--Select Action--</option>
                              <option>Verified</option>
                              <option>Bounce Check</option>
                              <option>Error on Check</option>
                              <option>Delete</option>
                            </select>
                            <div class="input-group-btn">
                              <button type="button" class="btn btn-info">Go</button>
                            </div>
                            <!-- /btn-group -->
                          </div>
                        </div>
                        
                      </form>
                    </div>
                    <!-- /row --> 
                    <div class="row">
                      <div class="col-md-12">
                        <table id="tbl_payments_list" class="table table-bordered table-striped">
                          <thead>
                          <tr>
                            <th>#</th>
                            <th>Date</th>
                            <th>PO No</th>
                            <th>Type</th>
                            <th>Check No</th>
                            <th>Bank</th>
                            <th>Branch</th>
                            <th>Amount</th>
                            <th>Status</th>
                          </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td><input type="checkbox"></td>
                              <td>01/01/2015</td>
                              <td>AC-124</td>
                              <td>Check</td>
                              <td>123-1145-1234-53-4</td>
                              <td>BPI</td>
                              <td>Ortigas - Emerald</td>
                              <td>P100,000.00</td>
                              <td>For Verification</td>
                            </tr>
                            <tr>
                              <td><input type="checkbox"></td>
                              <td>01/01/2015</td>
                              <td>AC-124</td>
                              <td>Check</td>
                              <td>123-1145-1234-53-4</td>
                              <td>BPI</td>
                              <td>Ortigas - Emerald</td>
                              <td>P100,000.00</td>
                              <td>For Verification</td>
                            </tr>
                            <tr>
                              <td><input type="checkbox"></td>
                              <td>01/01/2015</td>
                              <td>AC-124</td>
                              <td>Check</td>
                              <td>123-1145-1234-53-4</td>
                              <td>BPI</td>
                              <td>Ortigas - Emerald</td>
                              <td>P100,000.00</td>
                              <td>For Verification</td>
                            </tr>
                            <tr>
                              <td><input type="checkbox"></td>
                              <td>01/01/2015</td>
                              <td>AC-124</td>
                              <td>Check</td>
                              <td>123-1145-1234-53-4</td>
                              <td>BPI</td>
                              <td>Ortigas - Emerald</td>
                              <td>P100,000.00</td>
                              <td>For Verification</td>
                            </tr>
                            <tr>
                              <td><input type="checkbox"></td>
                              <td>01/01/2016</td>
                              <td>AC-124</td>
                              <td>Check</td>
                              <td>123-1145-1234-53-4</td>
                              <td>BPI</td>
                              <td>Ortigas - Onyx</td>
                              <td>P100,000.00</td>
                              <td>For Verification</td>
                            </tr>
                            <tr>
                              <td><input type="checkbox"></td>
                              <td>01/01/2015</td>
                              <td>AC-124</td>
                              <td>Check</td>
                              <td>123-1145-1234-53-4</td>
                              <td>BPI</td>
                              <td>Ortigas - Pearl</td>
                              <td>P100,000.00</td>
                              <td>For Verification</td>
                            </tr>
                            <tr>
                              <td><input type="checkbox"></td>
                              <td>01/01/2015</td>
                              <td>AC-124</td>
                              <td>Check</td>
                              <td>123-1145-1234-53-4</td>
                              <td>BPI</td>
                              <td>Ortigas - Emerald</td>
                              <td>P100,000.00</td>
                              <td>For Verification</td>
                            </tr>
                            <tr>
                              <td><input type="checkbox"></td>
                              <td>01/01/2015</td>
                              <td>AC-124</td>
                              <td>Check</td>
                              <td>123-1145-1234-53-4</td>
                              <td>BPI</td>
                              <td>Ortigas - Sapphire</td>
                              <td>P100,000.00</td>
                              <td>For Verification</td>
                            </tr>
                          
                          </tbody>
                          <tfoot>
                          <tr>
                          <th>#</th>
                            <th>Date</th>
                            <th>PO No</th>
                            <th>Type</th>
                            <th>Check No</th>
                            <th>Bank</th>
                            <th>Branch</th>
                            <th>Amount</th>
                            <th>Status</th>
                          </tr>
                          </tfoot>
                        </table>
                      </div>
                        
                    </div>
                    
                  </div>
                </div>                 
                <!-- /Purchase Item Table -->

                <!-- Remove From List -->
                <div class="row">
                  <div class="col-md-4">
                    <table class="table table-bordered">
                      <tr>
                        <th colspan="2">Payment Summary</th>
                      </tr>
                      <tr> 
                        <th>Total Cash Amount</th>
                        <td>P 100,000.00</td>
                      </tr>
                      <tr>
                        <th>Total Check Amount</th>
                        <td>P 100,000.00</td>
                      </tr>
                      <tr>
                        <th>Total Payment Amount</th>
                        <td>P 200,000.00</td>
                      </tr>
                      </tr>
                    </table>

                  </div>
                  <div class="col-md-4">
                    <table class="table table-bordered">
                      <tr>
                        <th colspan="2">Purchase Summary</th>
                      </tr>
                      <tr> 
                        <th>Total Transaction Amount</th>
                        <td>P 500,000.00</td>
                      </tr>
                      <tr>
                        <th>Total Payment Amount</th>
                        <td>P 200,000.00</td>
                      </tr>
                      <tr>
                        <th>Total Balance</th>
                        <td>P 300,000.00</td>
                      </tr>
                      </tr>
                    </table>

                  </div>

                  <div class="col-md-4">
                      <div class="row">
                        <div class="col-md-12">
                          <button class="btn bg-orange pull-right" style="margin-left:1%"> Clear Form</button> 
                          <button class="btn btn-success pull-right"> Save and Print Invoice</button> 
                          
                        </div>
                      </div>
                      <!-- /btn-group -->
                  </div>

                </div>
                <!-- /. Remove From List -->

              </div>
              <!-- /.box-body -->
            </form>
            <!-- /form-end -->
          </div>
          <!-- /.box -->
        <!--/.col (left) -->
        </div>
      <!-- search form -->
      </div>
      <!-- /Purchased Items -->



    </section>
@endsection

@section('additional_footer')

<script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
<script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>

<script src='{{ asset("/bower_components/AdminLTE/plugins/jQueryUI/jquery-ui.min.js")}}'></script>
<script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>
<script src='{{ asset("/bower_components/AdminLTE/plugins/iCheck/icheck.min.js")}}'></script>

<script>
  $(document).ready(function() {  

    $('#txt_check_date, #txt_purchase_date').daterangepicker({
      singleDatePicker: true,
      showDropdowns: true
    });

    $("#slct_payment_type").change(function(){
      if($(this).val() == 'cash'){
        $("#div_cash").show();
        $("#div_check").hide();
      }else{
        $("#div_cash").hide();
        $("#div_check").show();
      }
    });

    //iCheck for checkbox and radio inputs
    $('input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });

    $('#tbl_payments_list').DataTable({
      "paging": false,
      "lengthChange": true,
      "searching": false,
      "ordering": true,
      "info": false,
      "autoWidth": true
    });

  });
</script>
@endsection