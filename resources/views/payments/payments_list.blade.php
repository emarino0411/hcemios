@extends('admin_template')

@section('additional_header')

<!-- DataTables -->
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>

@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12 col-xs-12">
          <!-- general form elements -->
          <div class="com-md-12 box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Search Purchase Order</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <div class="box-body">
              <div class="row">
                <form role="form">
                  <div class="col-md-2 col-sm-2">
                    <button id="btn_search" type="button" class="btn btn-primary" data-toggle="modal" data-target="#search-purchase"><i class="ion ion-search"></i> Search Payment Records </button>
                  </div>
                  <div class="col-md-2 col-sm-2 col-md-offset-5 col-sm-offset-5">
                    <a href="payments/addNew" class="btn btn-success pull-right"><i class="fa fa-money"></i> Add New Payment</a>
                  </div>
                  <div class="col-md-3">
                   <div class="input-group">
                      <select class="form-control">
                        <option>--Select Action--</option>
                        <option>Verified</option>
                        <option>Bounce Check</option>
                        <option>Error on Check</option>
                      </select>
                      <div class="input-group-btn">
                        <button type="button" class="btn btn-info">Go</button>
                      </div>
                      <!-- /btn-group -->
                    </div>
                  </div>
                  
                </form>
              </div>
              <!-- /row -->

            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        <!--/.col (left) -->
        </div>
      <!-- search form -->
      </div>
      <!-- /.row (main row) -->

      <div class="row">
        <div class=" col-md-12 col-xs-12">
          <div class="box box-primary">
              <div class="box-header">
                <h3 class="box-title"><i class="fa fa-shopping-cart"></i> Payment Records</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>&nbsp;</th>
                    <th>PO No</th>
                    <th>Check No</th>
                    <th>Bank</th>
                    <th>Branch</th>
                    <th>Amount</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td><input type="checkbox"></td>
                      <td>AC-124</td>
                      <td>123-1145-1234-53-4</td>
                      <td>BPI</td>
                      <td>Ortigas - Emerald</td>
                      <td>P100,000.00</td>
                      <td>For Verification</td>
                      <td>View / Update</td>
                    </tr>
                    <tr>
                      <td><input type="checkbox"></td>
                      <td>AC-124</td>
                      <td>123-1145-1234-53-4</td>
                      <td>BPI</td>
                      <td>Ortigas - Emerald</td>
                      <td>P100,000.00</td>
                      <td>For Verification</td>
                      <td>View / Update</td>
                    </tr>
                    <tr>
                      <td><input type="checkbox"></td>
                      <td>AC-124</td>
                      <td>123-1145-1234-53-4</td>
                      <td>BPI</td>
                      <td>Ortigas - Emerald</td>
                      <td>P100,000.00</td>
                      <td>For Verification</td>
                      <td>View / Update</td>
                    </tr>
                    <tr>
                      <td><input type="checkbox"></td>
                      <td>AC-124</td>
                      <td>123-1145-1234-53-4</td>
                      <td>BPI</td>
                      <td>Ortigas - Emerald</td>
                      <td>P100,000.00</td>
                      <td>For Verification</td>
                      <td>View / Update</td>
                    </tr>
                    <tr>
                      <td><input type="checkbox"></td>
                      <td>AC-124</td>
                      <td>123-1145-1234-53-4</td>
                      <td>BPI</td>
                      <td>Ortigas - Emerald</td>
                      <td>P100,000.00</td>
                      <td>For Verification</td>
                      <td>View / Update</td>
                    </tr>
                    <tr>
                      <td><input type="checkbox"></td>
                      <td>AC-124</td>
                      <td>123-1145-1234-53-4</td>
                      <td>BPI</td>
                      <td>Ortigas - Emerald</td>
                      <td>P100,000.00</td>
                      <td>For Verification</td>
                      <td>View / Update</td>
                    </tr>
                    <tr>
                      <td><input type="checkbox"></td>
                      <td>AC-124</td>
                      <td>123-1145-1234-53-4</td>
                      <td>BPI</td>
                      <td>Ortigas - Emerald</td>
                      <td>P100,000.00</td>
                      <td>For Verification</td>
                      <td>View / Update</td>
                    </tr>
                    <tr>
                      <td><input type="checkbox"></td>
                      <td>AC-124</td>
                      <td>123-1145-1234-53-4</td>
                      <td>BPI</td>
                      <td>Ortigas - Emerald</td>
                      <td>P100,000.00</td>
                      <td>For Verification</td>
                      <td>View / Update</td>
                    </tr>
                  
                  </tbody>
                  <tfoot>
                  <tr>
                  <th>#</th>
                    <th>PO No</th>
                    <th>Check No</th>
                    <th>Bank</th>
                    <th>Branch</th>
                    <th>Amount</th>
                    <th>Status</th>
                    <th>Action</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.box-body -->
            </div>
          </div>
        </div>
    </section>

@endsection

@section('additional_footer')

<script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
<script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>
<script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>


<script>
  $(document).ready(function() {
    $('#example1').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });

    $("#btn_search").click(function(){
      $(".select2").select2({
        theme:"classic"
      });
      $('#txt_daterange').daterangepicker();

    });

    $(".hide-filter").each(function(index) {  
      $(this).parent().parent().hide();
      $(this).on("click", function(){
        $(this).parent().parent().hide();
        $("."+$(this).parent().parent().attr('id')).show();
      });
    });

    $(".dropdown-menu li").each(function(index){
      $(this).on("click", function(){
        $("#"+$(this).children("input").val()).show();
        $(this).hide();
      });
    })
  });
</script>
@endsection