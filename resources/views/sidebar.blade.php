<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset("/bower_components/AdminLTE/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
				<span class="input-group-btn">
				  <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
				</span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li>
              <a href="/dashboard">
                <i class="fa fa-dashboard"></i> <span>HCEMI Dashboard</span></i>
              </a>  
            </li>
            <li>
              <a href="/purchases">
                <i class="fa fa-shopping-cart"></i> <span>Purchase Orders</span></i>
              </a>  
            </li>
            <li>
              <a href="/payments">
                <i class="fa fa-money"></i> <span>Payments</span></i>
              </a>  
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-rub"></i> <span>Commissions</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="/commissions/"><i class="fa fa-users"></i> Distribute Commissions</a></li>
                <li><a href="/commissions/voucher"><i class="fa fa-money"></i> Vouchers</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-book"></i> <span>Bookings</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li><a href="/bookings"><i class="fa fa-calendar"></i> Booking Calendar</a></li>
                <li><a href="/bookings/list"><i class="fa fa-list"></i> Booking List</a></li>
              </ul>
            </li>
            <li>
              <a href="/clients">
                <i class="fa fa-briefcase"></i> <span>Clients</span></i>
              </a>  
            </li>
            <li>
              <a href="/agents">
                <i class="fa fa-users"></i> <span>Agents</span></i>
              </a>  
            </li>
             <li>
              <a href="/products">
                <i class="fa fa-cutlery"></i> <span>Products and Sets</span></i>
              </a>  
            </li>
            <li>
              <a href="/promos">
                <i class="fa fa-gift"></i> <span>Promos</span></i>
              </a>  
            </li>
           
            <li>
              <a href="/reports">
                <i class="fa fa-line-chart"></i> <span>Reports</span></i>
              </a>  
            </li>

            <li>
                <a href="/users">
                    <i class="fa fa-street-view"></i> <span>Users</span></i>
                </a>
            </li>

            <li>
              <a href="/roles">
                <i class="fa fa-key"></i> <span>User Roles</span></i>
              </a>  
            </li>
            
            
            
      </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>