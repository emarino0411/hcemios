@extends('admin_template')

@section('additional_header')

<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/iCheck/all.css")}}'>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
<style>
  /*
Force table width to 100%
*/
 table.table-fixedheader {

}
/*
Set table elements to block mode.  (Normally they are inline).
This allows a responsive table, such as one where columns can be stacked
if the display is narrow.
*/
 table.table-fixedheader, table.table-fixedheader>thead, table.table-fixedheader>tbody, table.table-fixedheader>thead>tr, table.table-fixedheader>tbody>tr, table.table-fixedheader>thead>tr>th, table.table-fixedheader>tbody>td {
    display: block;
}
table.table-fixedheader>thead>tr:after, table.table-fixedheader>tbody>tr:after {
    content:' ';
    display: block;
    visibility: hidden;
    clear: both;
}
/*
When scrolling the table, actually it is only the tbody portion of the
table that scrolls (not the entire table: we want the thead to remain
fixed).  We must specify an explicit height for the tbody.  We include
100px as a default, but it can be overridden elsewhere.

Also, we force the scrollbar to always be displayed so that the usable
width for the table contents doesn't change (such as becoming narrower
when a scrollbar is visible and wider when it is not).
*/
 table.table-fixedheader>tbody {
    overflow-y: scroll;
    height: 100px;
    
}
/*
We really don't want to scroll the thead contents, but we want to force
a scrollbar to be displayed anyway so that the usable width of the thead
will exactly match the tbody.
*/
 table.table-fixedheader>thead {
    overflow-y: scroll;    
}
/*
For browsers that support it (webkit), we set the background color of
the unneeded scrollbar in the thead to make it invisible.  (Setting
visiblity: hidden defeats the purpose, as this alters the usable width
of the thead.)
*/
 table.table-fixedheader>thead::-webkit-scrollbar {
    background-color: inherit;
}


table.table-fixedheader>thead>tr>th:after, table.table-fixedheader>tbody>tr>td:after {
    content:' ';
    display: table-cell;
    visibility: hidden;
    clear: both;
}

/*
We want to set <th> and <td> elements to float left.
We also must explicitly set the width for each column (both for the <th>
and the <td>).  We set to 20% here a default placeholder, but it can be
overridden elsewhere.
*/

 table.table-fixedheader>thead tr th, table.table-fixedheader>tbody tr td {
    float: left;    
    word-wrap:break-word;     
}






</style>
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">

      <!-- Client Information -->
      <div class="row">
        <div class=" col-md-12 col-xs-12">
          <div class="row">
            <!-- client -->
            <div class="col-md-12">              
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title"><i class="fa fa-user"></i> Search Client</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <div class="row">
                    <div class="col-md-12">
                      <form class="form-horizontal">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="txt_po_no" class="col-sm-4 control-label">Commissions For</label>
                            <div class="col-sm-6">
                                <div class="input-group">
                                  <div class="input-group-addon">
                                    <i class="fa fa-user"></i>
                                  </div>
                                  <input type="text" class="form-control pull-right" id="txt_po_no">
                                </div>
                                <!-- /.input group -->
                            </div>  
                            <div class="col-md-2">
                              <button id="btn_search" type="button" class="btn btn-primary" data-toggle="modal" data-target="#search-purchase"><i class="ion ion-search"></i> Search Client </button>
                            </div>
                          </div>
                        </div>
                      
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="txt_po_no" class="col-sm-4 control-label">Select PO No</label>
                            <div class="col-sm-8">
                                <div class="input-group">
                                  <div class="input-group-addon">
                                    <i class="fa fa-edit"></i>
                                  </div>
                                  <select class="form-control">
                                    <option>PO-1545</option>
                                    <option>PO-1546</option>
                                    <option>PO-1547</option>
                                  </select>
                                </div>
                                <!-- /.input group -->
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
                <!-- /.box-body -->
              </div>              
            </div>
            <!-- /client -->

          </div>
        </div>
      </div>
      <!-- / client information --> 

      <!-- Purchase Information -->
      <div class="row">
        <div class=" col-md-12 col-xs-12">
          <div class="row">
            <!-- client -->
            <div class="col-md-12">              
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title"><i class="fa fa-user"></i> Client Information</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                  <div class="col-md-12">
                    <div class="col-md-2">
                      <!-- Profile Image -->
                      <img class="profile-user-img img-responsive img-circle" src='{{ asset ("/bower_components/AdminLTE/dist/img/user4-128x128.jpg") }}' style="margin-top:3%" alt="User profile picture">

                      <h3 class="profile-username text-center">Nina Mcintire</h3>                      
                    </div>
                    <div class="col-md-3">
                     <div class="box-header with-border">
                        <h3 class="box-title">Purchase Order</h3>
                      </div>
                      <!-- /.box-header -->
                      <div class="box-body">
                        <strong><i class="fa fa-edit"></i> PO Number</strong>
                        <p class="text-muted">
                         PO-123
                        </p>

                        <strong><i class="fa fa-user"></i> Client Name</strong>
                        <p class="text-muted">
                         Nina McIntire
                        </p>

                        <strong><i class="fa fa-calendar"></i> Sales Date</strong>
                        <p class="text-muted">
                        December 25,2015
                        </p>
                      </div>
                      <!-- /.box-body -->
                    </div>
                    <div class="col-md-3">
                      <div class="box-header with-border">
                        <h3 class="box-title">Payment Information</h3>
                      </div>
                      <!-- /.box-header -->
                      <div class="box-body">
                        <strong><i class="fa fa-list-alt"></i> Terms</strong>
                        <p class="text-muted">
                         PO-123
                        </p>

                        <strong><i class="fa fa-forward"></i> Is Fast Track</strong>
                        <p class="text-muted">
                        No
                        </p>

                        <strong><i class="fa fa-calendar"></i> Delivery Date</strong>
                        <p class="text-muted">
                        December 25,2015
                        </p>
                      </div>
                      <!-- /.box-body -->
                    </div>
                    <div class="col-md-4">
                      <div class="box-header with-border">
                        <h3 class="box-title">Purchased Items</h3>
                      </div>
                      <!-- /.box-header -->
                      <div class="box-body">
                        <table class="table table-fixedheader table-bordered" style="width:200%">
                          <thead>
                            <tr>
                              <th width="10%">#</th>
                              <th width="70%">Item Name</th>
                              <th width="20%">Gift</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td width="10%">1.</td>
                              <td width="70%">Professional Set</td>
                              <td width="20%"><span class="label label-success">Yes</span></td>
                            </tr>
                            <tr>
                              <td width="10%">1.</td>
                              <td width="70%">Professional Set</td>
                              <td width="20%"><span class="label label-success">Yes</span></td>
                            </tr>
                            <tr>
                              <td width="10%">1.</td>
                              <td width="70%">Professional Set</td>
                              <td width="20%"><span class="label label-success">Yes</span></td>
                            </tr>
                            <tr>
                              <td width="10%">1.</td>
                              <td width="70%">Professional Set</td>
                              <td width="20%"><span class="label label-success">Yes</span></td>
                            </tr>
                            <tr>
                              <td width="10%">1.</td>
                              <td width="70%">Professional Set</td>
                              <td width="20%"><span class="label label-success">Yes</span></td>
                            </tr>
                            <tr>
                              <td width="10%">1.</td>
                              <td width="70%">Professional Set</td>
                              <td width="20%"><span class="label label-success">Yes</span></td>
                            </tr>
                          </tbody>

                        </table>
                      </div>
                      <!-- /.box-body -->
                    </div>
                  </div>
                </div>
                <!-- /.box-body -->
              </div>              
            </div>
            <!-- /client -->

          </div>
        </div>
      </div>
      <!-- / client information --> 

      <!-- Agent Information -->
      <div class="row">
        <div class=" col-md-12 col-xs-12">
          <div class="row">
            <!-- client -->
            <div class="col-md-12">              
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title"><i class="fa fa-user"></i> Commission Information</h3>
                </div>
                <!-- /.box-header -->
                 <div class="box-body">
                    <div class="row">
                      <div class="col-md-12">
                        <form class="form-horizontal">
                          <!-- Commission Agent -->
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="slct_agent" class="col-sm-4 control-label">Select Agent</label>
                              <div class="col-sm-6">
                                  <div class="input-group">
                                    <div class="input-group-addon">
                                      <i class="fa fa-user"></i>
                                    </div>
                                    <select class="form-control" id="slct_agent">
                                      <option>Juan Dela Cruz</option>
                                      <option>Ruel Dela Jeu</option>
                                    </select>
                                    
                                  </div>
                                  <!-- /.input group -->                                  
                              </div>
                              <div class="col-sm-2">
                                <button id="btn_search" type="button" class="btn btn-primary" ><i class="ion ion-search"></i> Select Agent </button>
                              </div>
                            </div>                          
                          </div>
                          <!-- Commission Agent -->

                          <!-- Release Date -->
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="txt_check_date" class="col-sm-4 control-label">Release Date</label>
                              <div class="col-sm-8">
                                  <div class="input-group">
                                    <div class="input-group-addon">
                                      <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" id="txt_check_date">
                                  </div>
                                  <!-- /.input group -->
                              </div>
                            </div>
                          </div>
                          <!-- Release Date -->

                          <div class="col-md-12">
                            <hr>
                          </div>

                          <!-- Commissions Percentage -->
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="txt_commission_percentage" class="col-sm-4 control-label"> Commission Percentage</label>
                              <div class="col-sm-8">
                                  <div class="input-group">
                                    <div class="input-group-addon">
                                      <i class="fa fa-percent"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" value="5%" readonly style="background-color:white" id="txt_commission_percentage">
                                  </div>
                                  <!-- /.input group -->
                              </div>
                            </div>
                          </div>
                          <!-- Commissions Percentage -->

                          <!-- Commissions Amount -->
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="txt_commission_amount" class="col-sm-4 control-label"> Commission Amount</label>
                              <div class="col-sm-8">
                                  <div class="input-group">
                                    <div class="input-group-addon">
                                      <i class="fa fa-rub"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" value="P 100,000,000.00"  readonly style="background-color:white" id="txt_commission_amount">
                                  </div>
                                  <!-- /.input group -->
                              </div>
                            </div>
                          </div>
                          <!-- Commissions Amount -->

                          <div class="col-md-12">
                            <hr style="margin-bottom:1%">
                          </div>

                          <div class="col-md-12">
                            <h4 class="box-title"> <i class="fa fa-minus-square"></i> Deductions</h4>
                          </div>

                          <!-- Account Balance -->
                           <div class="col-md-12">
                            <div class="form-group">
                              <label for="txt_account_balance" class="col-sm-3 control-label"> Account Balance</label>
                              <div class="col-sm-3">
                                  <div class="input-group">
                                    <div class="input-group-addon">
                                      <i class="fa fa-percent"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" value="5" id="txt_account_balance">
                                  </div>
                                  <!-- /.input group -->
                              </div>
                              <div class="col-sm-5">
                                  <div class="input-group">
                                    <div class="input-group-addon">
                                      <i class="fa fa-rub"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" value="P 100,000,000.00"  readonly style="background-color:white"  id="txt_account_balance_amount">
                                  </div>
                                  <!-- /.input group -->
                              </div>
                            </div>
                          </div>
                          <!-- Account Balance -->

                          <!-- WSP -->
                           <div class="col-md-12">
                            <div class="form-group">
                              <label for="txt_wsp" class="col-sm-3 control-label"> WSP</label>
                              <div class="col-sm-3">
                                  <div class="input-group">
                                    <div class="input-group-addon">
                                      <i class="fa fa-percent"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" value="5" id="txt_wsp">
                                  </div>
                                  <!-- /.input group -->
                              </div>
                              <div class="col-sm-5">
                                  <div class="input-group">
                                    <div class="input-group-addon">
                                      <i class="fa fa-rub"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" value="P 100,000,000.00"  readonly style="background-color:white" id="txt_wsp_amount">
                                  </div>
                                  <!-- /.input group -->
                              </div>
                            </div>
                          </div>
                          <!-- WSP -->


                          <!-- Others -->
                           <div class="col-md-12">
                            <div class="form-group">
                              <label for="txt_wsp" class="col-sm-3 control-label"> Description</label>
                              <div class="col-sm-3">
                                  <div class="input-group">
                                    <div class="input-group-addon">
                                      <i class="fa fa-edit"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" value="Cash Advance" id="txt_others_desc">
                                  </div>
                                  <!-- /.input group -->
                              </div>
                              <div class="col-sm-5">
                                  <div class="input-group">
                                    <div class="input-group-addon">
                                      <i class="fa fa-rub"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" value="P 100,000,000.00" id="txt_others_amount">
                                  </div>
                                  <!-- /.input group -->
                              </div>
                            </div>
                          </div>
                          <!-- Others -->

                        </form>

                        <!-- buttons  -->
                        <div class="col-md-4 col-md-offset-8">
                            <div class="row">
                              <div class="col-md-12">
                                <button class="btn btn-info pull-right"> Save And Print</button>
                                <button class="btn btn-success pull-right" style="margin-right:1%"> Save and Exit</button> 
                                <button class="btn bg-orange pull-right" style="margin-right:1%"> Clear Form</button>                          
                              </div>
                            </div>
                            <!-- /btn-group -->
                        </div>
                        <!-- /.buttons  -->
                     
                      </div><!-- col-md-12 -->
                    </div><!-- /row -->
                  </div>
                <!-- /.box-body -->
              </div>              
            </div>
            <!-- /client -->

          </div>
        </div>
      </div>
      <!-- / Agent information --> 


      
    </section>
@endsection

@section('additional_footer')

<script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
<script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>

<script src='{{ asset("/bower_components/AdminLTE/plugins/jQueryUI/jquery-ui.min.js")}}'></script>
<script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>
<script src='{{ asset("/bower_components/AdminLTE/plugins/iCheck/icheck.min.js")}}'></script>

<script>
  $(document).ready(function() {  

    $('#txt_check_date, #txt_purchase_date').daterangepicker({
      singleDatePicker: true,
      showDropdowns: true
    });

    $("#slct_payment_type").change(function(){
      if($(this).val() == 'cash'){
        $("#div_cash").show();
        $("#div_check").hide();
      }else{
        $("#div_cash").hide();
        $("#div_check").show();
      }
    });

    //iCheck for checkbox and radio inputs
    $('input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });

    $('#tbl_payments_list').DataTable({
      "paging": false,
      "lengthChange": true,
      "searching": false,
      "ordering": true,
      "info": false,
      "autoWidth": true
    });

  });
</script>
@endsection