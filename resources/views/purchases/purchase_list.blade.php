@extends('admin_template')

@section('additional_header')

        <!-- DataTables -->
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>

@endsection

@section('content')
        <!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <!-- general form elements -->
            <div class="com-md-12 box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Search Purchase Order</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                    <div class="row">
                        <form role="form">
                            <div class="col-md-2 col-sm-2">
                                <button id="btn_search" type="button" class="btn btn-primary" data-toggle="modal"
                                        data-target="#search-purchase"><i class="ion ion-search"></i> Search Purchase
                                    Order
                                </button>
                            </div>
                            <div class="col-md-2 col-sm-2 col-md-offset-5 col-sm-offset-5">
                                <a href="purchases/addNew" class="btn btn-success"><i class="fa fa-shopping-cart"></i>
                                    New Purchase Order</a>
                            </div>
                            <div class="col-md-3">
                                <div class="input-group">
                                    <select class="form-control">
                                        <option>--Select Action--</option>
                                        <option>Archive</option>
                                        <option>Payment Completed</option>
                                        <option>For Pull Out</option>
                                        <option>Cancel PO</option>
                                    </select>

                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-info">Go</button>
                                    </div>
                                    <!-- /btn-group -->
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box -->
            <!--/.col (left) -->
        </div>
        <!-- search form -->
    </div>
    <!-- /.row (main row) -->

    <div class="row">
        <div class=" col-md-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title"><i class="fa fa-shopping-cart"></i> Purchase Order Records</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>PO No</th>
                            <th>Client Name</th>
                            <th>Date Purchased</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($purchases as $purchase)
                            <tr>
                                <td><input type="checkbox"></td>
                                <td>{{$purchase->id}}</td>
                                <td>{{$purchase->first_name}} {{$purchase->last_name}}</td>
                                <td>{{$purchase->purchase_date}}</td>
                                <td>{{$purchase->transaction_status}}</td>
                                <td>
                                    <a href="purchase/viewPurchaseDetails/{{$purchase->id}}"
                                       data-toggle="tooltip"
                                       title="View {{$purchase->first_name}} {{$purchase->last_name}}'s purchase details"
                                       class="btn btn-success btn-xs">
                                        <i class="fa fa-info-circle"></i> View
                                    </a>
                                    <a href="users/editProfile/{{$purchase->id}}/edit"
                                       data-toggle="tooltip"
                                       title="Edit {{$purchase->first_name}} {{$purchase->last_name}}'s purchase details"
                                       class="btn btn-primary btn-xs">
                                        <i class="fa fa-edit"></i> Edit
                                    </a>
                                    @if($purchase->status=='ACTIVE')
                                        <button type="button" class="btn btn-danger btn-xs deactivate-user"
                                                data-toggle="modal"
                                                data-id="{{$purchase->id}}"
                                                data-target="#delete-user"><i class="fa fa-warning"></i> Deactivate
                                        </button>
                                    @else
                                        <button type="button" class="btn btn-warning btn-xs activate-user"
                                                data-toggle="modal"
                                                data-id="{{$purchase->id}}"
                                                data-target="#activate-user"><i class="fa fa-power-off"></i> Activate
                                        </button>
                                    @endif

                                    {{--<a href="users/deleteProfile/{{$purchase->id}}"--}}
                                    {{--data-toggle="tooltip"--}}
                                    {{--title="Deactivate {{$purchase->first_name}} {{$purchase->last_name}}'s details"--}}
                                    {{--class="btn btn-warning btn-xs"--}}
                                    {{--data-toggle="modal"--}}
                                    {{--data-target="#search-purchase">--}}
                                    {{--<i class="fa fa-warning"></i> Deactivate--}}
                                    {{--</a>--}}
                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                        <tfoot>
                        <tr>
                            <th>#</th>
                            <th>PO No</th>
                            <th>Client</th>
                            <th>Date Purchased</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</section>

@endsection

@section('additional_footer')

    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>


    <script>
        $(document).ready(function () {
            $('#example1').DataTable({
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });

            $("#btn_search").click(function () {
                $(".select2").select2({
                    theme: "classic"
                });
                $('#txt_daterange').daterangepicker();

            });

            $(".hide-filter").each(function (index) {
                $(this).parent().parent().hide();
                $(this).on("click", function () {
                    $(this).parent().parent().hide();
                    $("." + $(this).parent().parent().attr('id')).show();
                });
            });

            $(".dropdown-menu li").each(function (index) {
                $(this).on("click", function () {
                    $("#" + $(this).children("input").val()).show();
                    $(this).hide();
                });
            })
        });
    </script>
@endsection