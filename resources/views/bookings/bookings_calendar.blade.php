@extends('admin_template')

@section('additional_header')

<!-- DataTables -->
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/fullcalendar/fullcalendar.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/fullcalendar/fullcalendar.print.css")}}' media="print">

@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-12 col-xs-12">

          <div class="row">
            <div class="col-md-3">

              <div class="box box-solid">
                <div class="box-header with-border">
                  <h4 class="box-title">Search Bookings</h4>
                </div>
                <div class="box-body">
                  <button id="btn_search" type="button" class="btn btn-primary" data-toggle="modal" data-target="#search-purchase"><i class="ion ion-search"></i> Search Client </button>
                </div>
                <!-- /.box-body -->
              </div>

              <div class="box box-solid">
                <div class="box-header with-border">
                  <h4 class="box-title">Booking Events</h4>
                </div>
                <div class="box-body">
                  <!-- the events -->
                  <div id="external-events">
                    <div class="checkbox">
                      <label for="drop-remove">
                        <input type="checkbox" id="drop-remove" checked>
                        remove after drop
                      </label>
                    </div>
                  </div>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /. box -->
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Create Booking Event</h3>
                </div>
                <div class="box-body">
                  <div class="btn-group" style="width: 100%; margin-bottom: 10px;">
                    <!--<button type="button" id="color-chooser-btn" class="btn btn-info btn-block dropdown-toggle" data-toggle="dropdown">Color <span class="caret"></span></button>-->
                    <ul class="fc-color-picker" id="color-chooser">
                      <li><a class="text-aqua" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-blue" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-light-blue" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-teal" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-yellow" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-orange" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-green" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-lime" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-red" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-purple" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-fuchsia" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-muted" href="#"><i class="fa fa-square"></i></a></li>
                      <li><a class="text-navy" href="#"><i class="fa fa-square"></i></a></li>
                    </ul>
                  </div>
                  {{--<!-- /btn-group -->--}}
                  {{--<div class="input-group">--}}
                    {{--<input id="new-event" type="text" class="form-control" placeholder="Event Title">--}}

                    {{--<div class="input-group-btn">--}}
                      {{--<button id="add-new-event" type="button" class="btn btn-primary btn-flat">Add</button>--}}
                    {{--</div>--}}
                    {{--<!-- /btn-group -->--}}
                  {{--</div>--}}
                  {{--<!-- /input-group -->--}}

                  <div class="form-group">
                    <label for="txt_event_name">Event Name</label>
                    <input type="text" class="form-control" id="new-event" placeholder="Enter Event Name">
                  </div>
                  <div class="form-group">
                    <label for="txt_client_name">Client</label>
                    <input type="text" class="form-control" id="txt_client_name" placeholder="Enter Client Name">
                  </div>
                  <div class="form-group">
                    <label for="txt_representative">Representatives</label>
                    <input type="text" class="form-control" id="txt_representative" placeholder="Enter Representative Name">
                  </div>
                  <div class="form-group">
                    <label for="txt_street">Street</label>
                    <input type="text" class="form-control" id="txt_street" placeholder="Enter Street">
                  </div>
                  <div class="form-group">
                    <label for="txt_city">City</label>
                    <input type="text" class="form-control" id="txt_city" placeholder="Enter City">
                  </div>
                  <div class="form-group">
                    <label for="txt_province">Province</label>
                    <input type="text" class="form-control" id="txt_province" placeholder="Enter Province">
                  </div>
                  <div class="box-footer">
                    <button id="add-new-event" type="button" class="btn btn-primary btn-flat">Add Event</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.col -->
            <div class="col-md-9">
              <div class="box box-primary">
                <div class="box-body no-padding">
                  <!-- THE CALENDAR -->
                  <div id="calendar"></div>
                </div>
                <!-- /.box-body -->
              </div>
              <!-- /. box -->
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>      
      </div>
    </section>
    @include('modals')
@endsection

@section('additional_footer')

<script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
<script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>
<script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src='{{ asset("/bower_components/AdminLTE/plugins/fullcalendar/fullcalendar.min.js")}}'></script>

<script>
  $(document).ready(function() {
    $('#example1').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });

    $("#btn_search").click(function(){
      $(".select2").select2({
        theme:"classic"
      });
      $('#txt_daterange').daterangepicker();

    });

    $(".hide-filter").each(function(index) {  
      $(this).parent().parent().hide();
      $(this).on("click", function(){
        $(this).parent().parent().hide();
        $("."+$(this).parent().parent().attr('id')).show();
      });
    });

    $(".dropdown-menu li").each(function(index){
      $(this).on("click", function(){
        $("#"+$(this).children("input").val()).show();
        $(this).hide();
      });
    });
   

    /* initialize the external events
     -----------------------------------------------------------------*/
    function ini_events(ele) {
      ele.each(function () {

        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
        // it doesn't need to have a start or end
        var eventObject = {
          title: $.trim($(this).text()) // use the element's text as the event title
        };

        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject);

        // make the event draggable using jQuery UI
        $(this).draggable({
          zIndex: 1070,
          revert: true, // will cause the event to go back to its
          revertDuration: 0  //  original position after the drag
        });

      });
    }

    ini_events($('#external-events div.external-event'));

    /* initialize the calendar
     -----------------------------------------------------------------*/
    //Date for the calendar events (dummy data)
    var date = new Date();
    var d = date.getDate(),
        m = date.getMonth(),
        y = date.getFullYear();

    $('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,agendaWeek,agendaDay'
      },
      buttonText: {
        today: 'today',
        month: 'month',
        week: 'week',
        day: 'day'
      },
      //Random default events
      events: [
        @foreach($bookings as $booking)
        {
          title: "{{$booking->title}}",
          @if($booking->start == null)
            allDay: true,
            start: new Date("{{$booking->booking_date}}"),
          @else
            start: new Date("{{$booking->start}}"),
            end: new Date("{{$booking->end}}"),
          @endif

          backgroundColor: "{{$booking->backgroundColor}}", //red
          borderColor: "{{$booking->borderColor}}" //red
        }
        @if ($booking != end($bookings)) , @endif
        @endforeach
      ],
          //[
//        {
//          title: 'All Day Event',
//          start: new Date(y, m, 1),
//          backgroundColor: "#f56954", //red
//          borderColor: "#f56954" //red
//        },
//        {
//          title: 'Long Event',
//          start: new Date(y, m, d - 5),
//          end: new Date(y, m, d - 2),
//          backgroundColor: "#f39c12", //yellow
//          borderColor: "#f39c12" //yellow
//        },
//        {
//          title: 'Meeting',
//          start: new Date(y, m, d, 10, 30),
//          allDay: false,
//          backgroundColor: "#0073b7", //Blue
//          borderColor: "#0073b7" //Blue
//        },
//        {
//          title: 'Lunch',
//          start: new Date(y, m, d, 12, 0),
//          end: new Date(y, m, d, 14, 0),
//          allDay: false,
//          backgroundColor: "#00c0ef", //Info (aqua)
//          borderColor: "#00c0ef" //Info (aqua)
//        },
//        {
//          title: 'Birthday Party',
//          start: new Date(y, m, d + 1, 19, 0),
//          end: new Date(y, m, d + 1, 22, 30),
//          allDay: false,
//          backgroundColor: "#00a65a", //Success (green)
//          borderColor: "#00a65a" //Success (green)
//        },
//        {
//          title: 'Click for Google',
//          start: new Date(y, m, 28),
//          end: new Date(y, m, 29),
//          url: 'http://google.com/',
//          backgroundColor: "#3c8dbc", //Primary (light-blue)
//          borderColor: "#3c8dbc" //Primary (light-blue)
//        }
      //],
      editable: true,
      eventResize:function(event){
        var booking_date = $.fullCalendar.moment(event.start).format();

        var _date = booking_date.substring(0,booking_date.indexOf("T"));
        var time_start = booking_date.substring(booking_date.indexOf("T")+1,booking_date.length);

        var title = event.title;
        var id = title.substring(1,title.indexOf(":"));

        var defaultDuration = moment.duration($('#calendar').fullCalendar('option', 'defaultTimedEventDuration')); // get the default and convert it to proper type
        var end = event.end || event.start.clone().add(defaultDuration); // If there is no end, compute it
        var time_end = end.format().substring(end.format().indexOf("T")+1,booking_date.length);

        $.post("/bookings/updateDate", {
          id: id,
          booking_date: _date,
          booking_time: time_start,
          booking_time_end: time_end
        }).done(function (data) {
          console.log("Update date ok!");
        });
      },
      eventDrop:function(event){
        var booking_date = $.fullCalendar.moment(event.start).format();
        console.log(booking_date.indexOf("T"));
        var _date;
        var time_start;
        if(booking_date.indexOf("T")> -1){
          _date = booking_date.substring(0,booking_date.indexOf("T"));
        }else{
          _date = booking_date;
        }
        if(booking_date.indexOf("T")!=-1){
          time_start = booking_date.substring(booking_date.indexOf("T")+1,booking_date.length);
          var defaultDuration = moment.duration($('#calendar').fullCalendar('option', 'defaultTimedEventDuration')); // get the default and convert it to proper type
          var end = event.end || event.start.clone().add(defaultDuration); // If there is no end, compute it
          var time_end = end.format().substring(end.format().indexOf("T")+1,booking_date.length);
        }else{
          time_start = '00:00:00';
          time_end = '02:00:00';
        }
        console.log(time_start);
        console.log(time_end);
        var title = event.title;
        var id = title.substring(1,title.indexOf(":"));

        $.post("/bookings/updateDate", {
          id: id,
          booking_date: _date,
          booking_time: time_start,
          booking_time_end: time_end
        }).done(function (data) {
          console.log("Update date ok!");
        });
      },
      droppable: true, // this allows things to be dropped onto the calendar !!!
      drop: function (date, allDay) { // this function is called when something is dropped

        // retrieve the dropped element's stored Event Object
        var originalEventObject = $(this).data('eventObject');

        // we need to copy it, so that multiple events don't have a reference to the same object
        var copiedEventObject = $.extend({}, originalEventObject);

        // assign it the date that was reported
        copiedEventObject.start = date;
        copiedEventObject.allDay = allDay;
        copiedEventObject.backgroundColor = $(this).css("background-color");
        copiedEventObject.borderColor = $(this).css("border-color");

        var title = copiedEventObject.title;
        var id = title.substring(1,title.indexOf(":"));

        $.post("/bookings/updateDate", {
          id: id,
          booking_date: copiedEventObject.start.format()
        }).done(function (data) {
          console.log("Update date ok!");
        });
        copiedEventObject.id = id;
        // render the event on the calendar
        // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
        $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

        // is the "remove after drop" checkbox checked?
        if ($('#drop-remove').is(':checked')) {
          // if so, remove the element from the "Draggable Events" list
          $(this).remove();
        }

      },eventClick: function(calEvent, jsEvent, view) {
//
//        alert('Event: ' + calEvent.title);
//        alert('Event: ' + calEvent.start);
//        alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
//        alert('View: ' + view.name);

        var id = calEvent.title.substring(1,calEvent.title.indexOf(":"));
        $("#eventId").val(calEvent._id);
        $.post("/bookings/getBookingDetails", {
          bookingId: id
        }).done(function (result) {
          var data = result[0];
          $("#bookingId").val(id);
          $("#booking_event_name").html(data.event_name);
          $("#booking_client_name").html(data.client);
          $("#booking_representative_name").html(data.representative == null ? 'N/A' : data.representative);
          $("#booking_address").html(data.street + ' ' + data.city + ' ' + data.province);
          $('#view-booking').modal({
            show: 'true'
          });
        });

      }
    });

    /* ADDING EVENTS */
    var currColor = "#3c8dbc"; //Red by default
    //Color chooser button
    var colorChooser = $("#color-chooser-btn");
    $("#color-chooser > li > a").click(function (e) {
      e.preventDefault();
      //Save color
      currColor = $(this).css("color");
      //Add color effect to button
      $('#add-new-event').css({"background-color": currColor, "border-color": currColor});
    });
    $("#add-new-event").click(function (e) {
      e.preventDefault();
      //Get value and make sure it is not null
      var val = $("#new-event").val();
      if (val.length == 0) {
        return;
      }


      $.post("/bookings/create", {
        event_name: $("#new-event").val(),
        street: $("#txt_street").val(),
        city: $("#txt_city").val(),
        province: $("#txt_province").val(),
        client: $("#txt_client_name").val(),
        representative: $("#txt_representative").val(),
        color:currColor
      }).done(function (data) {
        //console.log(data);
        val = data;
        //Create events
        var event = $("<div />");
        event.css({"background-color": currColor, "border-color": currColor, "color": "#fff"}).addClass("external-event");
        event.html(val);
        $('#external-events').prepend(event);
        //Add draggable funtionality
        ini_events(event);

        //Remove event from text input
        $("#new-event").val("");
      });

    });

    $("#btn_cancel_update").click(function(){
      var bookingId = $("#bookingId").val();
      var eventId = $("#eventId").val();
      $('#calendar').fullCalendar('removeEvents',eventId);
      $.post("/bookings/cancelBooking", {
        bookingId: bookingId
      }).done(function (result) {
        $('#view-booking').modal('hide');
      });
    });
    
  });
</script>
@endsection