@extends('admin_template')

@section('additional_header')

<!-- DataTables -->
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker-bs3.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/fullcalendar/fullcalendar.min.css")}}'>
<link rel="stylesheet" href='{{ asset("/bower_components/AdminLTE/plugins/fullcalendar/fullcalendar.print.css")}}' media="print">

@endsection

@section('content')
    <!-- Main content -->
    <section class="content">

        <div class="row">
          <div class="col-md-12 col-xs-12">
            <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title"><i class="fa fa-search"></i> Search Bookings</h3>
                </div>
                <div class="box-body">
                  <div class="row">
                    <form role="form">
                      <div class="col-md-2 col-sm-2">
                        <button id="btn_search" type="button" class="btn btn-primary" data-toggle="modal" data-target="#search-purchase"><i class="ion ion-search"></i> Search Booking Records </button>
                      </div>
                      <div class="col-md-2 col-sm-2 col-md-offset-5 col-sm-offset-5">
                        <a href="payments/addNew" class="btn btn-success pull-right"><i class="fa fa-calendar"></i> Add New Booking</a>
                      </div>
                      <div class="col-md-3">
                       <div class="input-group">
                          <select class="form-control">
                            <option>--Select Action--</option>
                            <option>Verified</option>
                            <option>Cancelled</option>
                          </select>
                          <div class="input-group-btn">
                            <button type="button" class="btn btn-info">Go</button>
                          </div>
                          <!-- /btn-group -->
                        </div>
                      </div>
                      
                    </form>
                  </div>
                  <!-- /row -->
                </div>
              </div>
            <!-- /.box -->
          <!--/.col (left) -->
          </div>
        <!-- search form -->
        </div>

        <div class="row">
          <div class=" col-md-12 col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                  <h3 class="box-title"><i class="fa fa-calendar"></i> Booking Schedule</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                      <th>&nbsp;</th>
                      <th>Date</th>
                      <th>Time</th>
                      <th>Agent</th>
                      <th>Location</th>
                      <th>Status</th>
                      <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><input type="checkbox"></td>
                        <td>Jan. 19,2016</td>
                        <td>1:00PM</td>
                        <td>Rodriguez, Juan</td>
                        <td>Ortigas - Emerald</td>
                        <td>Unverified</td>
                        <td>View / Update</td>
                      </tr>
                      <tr>
                        <td><input type="checkbox"></td>
                        <td>Jan. 19,2016</td>
                        <td>1:00PM</td>
                        <td>Rodriguez, Juan</td>
                        <td>Ortigas - Emerald</td>
                        <td>Unverified</td>
                        <td>View / Update</td>
                      </tr>
                      <tr>
                        <td><input type="checkbox"></td>
                        <td>Jan. 19,2016</td>
                        <td>1:00PM</td>
                        <td>Rodriguez, Juan</td>
                        <td>Ortigas - Emerald</td>
                        <td>Unverified</td>
                        <td>View / Update</td>
                      </tr>
                      <tr>
                        <td><input type="checkbox"></td>
                        <td>Jan. 19,2016</td>
                        <td>1:00PM</td>
                        <td>Rodriguez, Juan</td>
                        <td>Ortigas - Emerald</td>
                        <td>Unverified</td>
                        <td>View / Update</td>
                      </tr>
                      <tr>
                        <td><input type="checkbox"></td>
                        <td>Jan. 19,2016</td>
                        <td>1:00PM</td>
                        <td>Rodriguez, Juan</td>
                        <td>Ortigas - Emerald</td>
                        <td>Unverified</td>
                        <td>View / Update</td>
                      </tr>
                      <tr>
                        <td><input type="checkbox"></td>
                        <td>Jan. 19,2016</td>
                        <td>1:00PM</td>
                        <td>Rodriguez, Juan</td>
                        <td>Ortigas - Emerald</td>
                        <td>Unverified</td>
                        <td>View / Update</td>
                      </tr>
                      <tr>
                        <td><input type="checkbox"></td>
                        <td>Jan. 19,2016</td>
                        <td>1:00PM</td>
                        <td>Rodriguez, Juan</td>
                        <td>Ortigas - Emerald</td>
                        <td>Unverified</td>
                        <td>View / Update</td>
                      </tr>
                      <tr>
                        <td><input type="checkbox"></td>
                        <td>Jan. 19,2016</td>
                        <td>1:00PM</td>
                        <td>Rodriguez, Juan</td>
                        <td>Ortigas - Emerald</td>
                        <td>Unverified</td>
                        <td>View / Update</td>
                      </tr>
                      <tr>
                        <td><input type="checkbox"></td>
                        <td>Jan. 19,2016</td>
                        <td>1:00PM</td>
                        <td>Rodriguez, Juan</td>
                        <td>Ortigas - Emerald</td>
                        <td>Unverified</td>
                        <td>View / Update</td>
                      </tr>
                      <tr>
                        <td><input type="checkbox"></td>
                        <td>Jan. 19,2016</td>
                        <td>1:00PM</td>
                        <td>Rodriguez, Juan</td>
                        <td>Ortigas - Emerald</td>
                        <td>Unverified</td>
                        <td>View / Update</td>
                      </tr>
                      <tr>
                        <td><input type="checkbox"></td>
                        <td>Jan. 19,2016</td>
                        <td>1:00PM</td>
                        <td>Rodriguez, Juan</td>
                        <td>Ortigas - Emerald</td>
                        <td>Unverified</td>
                        <td>View / Update</td>
                      </tr>
                    </tbody>
                    <tfoot>
                     <tr>
                        <th>&nbsp;</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Agent</th>
                        <th>Location</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
                <!-- /.box-body -->
              
            </div>
          </div>
        </div>
        <!-- /.tab-pane -->

    </section>

@endsection

@section('additional_footer')

<script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/jquery.dataTables.min.js")}}'></script>
<script src='{{ asset("/bower_components/AdminLTE/plugins/datatables/dataTables.bootstrap.min.js")}}'></script>
<script src='{{ asset("/bower_components/AdminLTE/plugins/select2/select2.full.min.js")}}'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src='{{ asset("/bower_components/AdminLTE/plugins/daterangepicker/daterangepicker.js")}}'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
<script src='{{ asset("/bower_components/AdminLTE/plugins/fullcalendar/fullcalendar.min.js")}}'></script>

<script>
  $(document).ready(function() {
    $('#example1').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": true
    });

    $("#btn_search").click(function(){
      $(".select2").select2({
        theme:"classic"
      });
      $('#txt_daterange').daterangepicker();

    });

    $(".hide-filter").each(function(index) {  
      $(this).parent().parent().hide();
      $(this).on("click", function(){
        $(this).parent().parent().hide();
        $("."+$(this).parent().parent().attr('id')).show();
      });
    });

    $(".dropdown-menu li").each(function(index){
      $(this).on("click", function(){
        $("#"+$(this).children("input").val()).show();
        $(this).hide();
      });
    });
   
    
  });
</script>
@endsection