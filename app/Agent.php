<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
  /**
 * The database table used by the model.
 *
 * @var string
 */
  protected $table = 'representative';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['representative_code', 'last_name', 'first_name', 'middle_name', 'profile_picture',
    'sponsor_id', 'sponsor_date', 'distributor_id', 'phase1_start', 'phase1_end', 'phase2_start', 'phase2_end',
    'birthdate', 'birthplace', 'civil_status', 'street', 'city', 'address_province', 'phone1', 'phone2', 'phone3',
    'email_address', 'tin_no', 'sss_no', 'can_drive', 'own_car', 'previous_employer', 'p_employer_telno',
    'p_employer_addr', 'spouse_last_name', 'spouse_first_name', 'no_of_dependents', 'active', 'level_id',
    'level_start', 'signature', 'total_production', 'total_account_balance', 'total_wsp'];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [];
}
