<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
  protected $table = 'bookings';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['street','city','province','client','booking_date','booking_time'];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = ['created_at','updated_at'];
}
