<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
  return Redirect::to('users/login');
});

Route::get('login', ['as' => 'login', function () {
  return Redirect::to('users/login');
}]);

Route::controllers([
  'auth' => 'Auth\AuthController',
  'password' => 'Auth\PasswordController'
]);

/*
LOGIN
*/
/* User Authentication */
Route::get('users/login', 'Auth\AuthController@getLogin');
Route::post('users/login', 'Auth\AuthController@postLogin');
Route::get('users/logout', 'Auth\AuthController@getLogout');

Route::get('users/register', 'Auth\AuthController@getRegister');
Route::post('users/register', 'Auth\AuthController@postRegister');

/* Authenticated users */
Route::group(['middleware' => 'auth'], function () {
  Route::get('users/dashboard', array('as' => 'dashboard', function () {
    return View('users.dashboard');
  }));
});

/*
Dashboard
*/
Route::get('home', function () {
  return Redirect::to('dashboard');
});
Route::get('dashboard', 'DashboardController@index');


Route::get('purchases','PurchaseController@index');
Route::get('purchases/addNew','PurchaseController@addNew');
Route::post('purchases/createPOItem','PurchaseController@createPOItem');
Route::post('purchases/savePO','PurchaseController@savePurchaseOrder');

Route::get('payments', function () {
  return view('payments/payments_list', [
    'page_title' => 'HCEMIOS Payments',
    'page_description' => 'Will it be check or cash? :)',
    'level' => 'Payments',
    'sub_level' => 'List',
    'icon' => 'fa-money'
  ]);
});

Route::get('payments/addNew', function () {
  return view('payments/payments_add', [
    'page_title' => 'HCEMIOS Payments',
    'page_description' => 'Now accepting cash and check!:)',
    'level' => 'Payments',
    'sub_level' => 'Add New Payment',
    'icon' => 'fa-money'
  ]);
});

Route::get('commissions', function () {
  return view('commissions/commissions_distribute', [
    'page_title' => 'HCEMIOS Commissions',
    'page_description' => 'Let us distribute the commissions :)',
    'level' => 'Commissions',
    'sub_level' => 'Distribute Commissions',
    'icon' => 'fa-rub'
  ]);
});

Route::get('commissions/voucher', function () {
  return view('commissions/commissions_vouchers', [
    'page_title' => 'HCEMIOS Commissions',
    'page_description' => 'Who will have the voucher for today? :)',
    'level' => 'Commissions',
    'sub_level' => 'Create Voucher',
    'icon' => 'fa-rub'
  ]);
});

Route::get('bookings/list', function () {
  return view('bookings/bookings_list', [
    'page_title' => 'HCEMIOS Bookings',
    'page_description' => 'Here are our schedules for today? :)',
    'level' => 'Bookings',
    'sub_level' => 'List',
    'icon' => 'fa-calendar'
  ]);
});


Route::get('promos', function () {
  return view('promos/promos', [
    'page_title' => 'HCEMIOS Purchase',
    'page_description' => 'Are we having a new Purchase Order Today? :)',
    'level' => 'Purchase',
    'sub_level' => 'List'
  ]);
});


Route::get('reports', function () {
  return view('reports/reports', [
    'page_title' => 'HCEMIOS Purchase',
    'page_description' => 'Are we having a new Purchase Order Today? :)',
    'level' => 'Purchase',
    'sub_level' => 'List'
  ]);
});

/*
 * Bookings
 * */
Route::get('bookings','BookingsController@calendarView');
Route::post('bookings/create','BookingsController@create');
Route::post('bookings/updateDate','BookingsController@updateDate');

Route::get('bookings/list','BookingsController@index');
Route::get('bookings/addNew','BookingsController@addNew');
Route::post('bookings/addNew','BookingsController@create');
Route::get('bookings/viewProfile/{id}','BookingsController@viewProfile');
Route::get('bookings/editProfile/{id}/{edit}', 'BookingsController@viewProfile');
Route::post('bookings/update', 'BookingsController@update');
Route::post('bookings/delete', 'BookingsController@delete');
Route::post('bookings/activate', 'BookingsController@activate');
Route::post('bookings/getBookingDetails', 'BookingsController@getBookingDetails');
Route::post('bookings/cancelBooking', 'BookingsController@cancelBooking');
/*
 * Clients
 * */
Route::get('clients','ClientsController@index');
Route::get('clients/addNew','ClientsController@addNew');
Route::post('clients/addNew','ClientsController@create');
Route::get('clients/viewProfile/{id}','ClientsController@viewProfile');
Route::get('clients/editProfile/{id}/{edit}', 'ClientsController@viewProfile');
Route::post('clients/update', 'ClientsController@update');
Route::post('clients/delete', 'ClientsController@delete');
Route::post('clients/activate', 'ClientsController@activate');
/*
 * Products
 * */
Route::get('products','ProductsController@index');
Route::get('products/addNew','ProductsController@addProduct');
Route::post('products/create','ProductsController@create');
Route::get('products/viewProduct/{id}','ProductsController@viewProduct');
Route::post('products/update','ProductsController@update');
Route::get('products/editProfile/{id}/{edit_mode}','ProductsController@viewProduct');
Route::post('products/delete','ProductsController@delete');
Route::post('products/activate','ProductsController@activate');
Route::post('products/getProductsByType','ProductsController@getProductsByType');
/*
 * Agents
 * */
Route::get('agents','AgentsController@index');
Route::get('agents/addNew','AgentsController@addAgent');
Route::get('agents/viewProfile/{id}', 'AgentsController@viewProfile');
Route::get('agents/editProfile/{id}/{edit}', 'AgentsController@viewProfile');
Route::post('agents/addNew','AgentsController@create');
Route::post('agents/update', 'AgentsController@update');
Route::post('agents/delete', 'AgentsController@delete');
Route::post('agents/activate', 'AgentsController@activate');
/*
 * Refs
 * */
Route::post('provinces','RefAddressController@loadProvince');
Route::post('municipalities','RefAddressController@loadMunicipality');
/*
 * Users Management
 * */
Route::get('users', 'UsersController@index');
Route::get('users/addUser', 'UsersController@addUser');
Route::get('users/viewProfile/{id}', 'UsersController@viewProfile');
Route::get('users/editProfile/{id}/{edit}', 'UsersController@viewProfile');
Route::post('users/addUser', 'UsersController@create');
Route::post('users/update', 'UsersController@update');
Route::post('users/delete', 'UsersController@delete');
Route::post('users/activate', 'UsersController@activate');


/*
 * Roles Route
 * */
Route::get('roles', 'RolesController@index');
Route::post('roles/addNew', 'RolesController@create');
Route::post('roles/update', 'RolesController@update');
Route::post('roles/getConfig', 'RolesController@getRoleConfig');