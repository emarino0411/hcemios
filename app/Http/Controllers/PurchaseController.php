<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Product;
use App\Purchase;
use App\PurchaseItem;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class PurchaseController extends Controller
{
    public function index()
    {
        $purchases = DB::table('purchase')
                ->leftJoin('client', 'client.id', '=', 'purchase.client_client_id')
                ->select('client.last_name', 'client.first_name', 'purchase.id', 'purchase.purchase_date', 'purchase.transaction_status','purchase.status')
                ->get();
        return view('purchases/purchase_list', [
                'purchases' => $purchases,
                'page_title' => 'HCEMIOS Purchase',
                'page_description' => 'Are we having a new Purchase Order Today? :)',
                'level' => 'Purchase',
                'sub_level' => 'List',
                'icon' => 'fa-shopping-cart'
        ]);
    }

    public function addNew()
    {
        $agents = Agent::all();
        $products = Product::where('type', '=', 'Single');
        $purchase = Purchase::all()->last();

        $purchase_id = $purchase == null ? 1 : $purchase->id + 1;


        return view('purchases/purchase_add', [
                'page_title' => 'HCEMIOS Purchase',
                'page_description' => 'Yes we are having a new Purchase Order Today!:)',
                'level' => 'Purchase',
                'sub_level' => 'New Purchase Order',
                'icon' => 'fa-shopping-cart',
                'agents' => $agents,
                'products' => $products,
                'purchase_id' => $purchase_id
        ]);

    }

    public function createPOItem()
    {
        $update = false;
        $purchase_order = Purchase::find(Input::get('hdn_po_no'));
        if (!$purchase_order) {
            $purchase_order = new Purchase();
            $purchase_order->id = Input::get('hdn_po_no');
            $purchase_order->po_no = Input::get('txt_po_no');
            $purchase_order->purchase_date = Input::get('txt_purchase_date');
            $purchase_order->terms = Input::get('slct_terms');
            $purchase_order->delivery_date = Input::get('txt_delivery_date');
            $purchase_order->total_amount = 0;
            $purchase_order->associate_id = Input::get('slct_associate');
            $purchase_order->sponsor_id = Input::get('slct_sponsor');
            $purchase_order->consultant_id = Input::get('slct_consultant');
            $purchase_order->fast_track = Input::get('is_fast_track');
            $purchase_order->status = Input::get('slct_trans_status');
            $purchase_order->remarks = Input::get('txtarea_remarks');
            $purchase_order->client_client_id = 1;
            $purchase_order->user_user_id = 1;
            $purchase_order->transaction_status = 'Pending';
            $purchase_order->save();
        }

            $purchase_item_list = PurchaseItem::where('item_id', '=', Input::get('slct_purchase_item'))
                    ->where('purchase_po_no', '=', $purchase_order->id)
                    ->where('gift', '=', Input::get('is_gift'))
                    ->first();

        if (!$purchase_item_list) {
            $purchase_item_list = new PurchaseItem();
            $purchase_item_list->gift = Input::get('is_gift');
            $purchase_item_list->item_price = Input::get('total_item_price');
            $purchase_item_list->item_id = Input::get('slct_purchase_item');
            $purchase_item_list->purchase_type = Input::get('slct_purchase_type');
            $purchase_item_list->purchase_po_no = $purchase_order->id;
            $purchase_item_list->qty = Input::get('txt_qty');
            $purchase_item_list->user_user_id = 1;
            $purchase_item_list->save();
        } else {

            $purchase_item_list->qty = $purchase_item_list->qty + Input::get('txt_qty');
            $purchase_item_list->item_price = $purchase_item_list->item_price + Input::get('total_item_price');
            $purchase_item_list->save();
            $update = true;
        }
        return [$purchase_item_list, $update];

    }

    public function savePurchaseOrder(){
        $purchase_order = Purchase::find(Input::get('hdn_po_no'));
        $purchase_order->po_no = Input::get('txt_po_no');
        $purchase_order->purchase_date = Input::get('txt_purchase_date');
        $purchase_order->terms = Input::get('slct_terms');
        $purchase_order->delivery_date = Input::get('txt_delivery_date');
        $purchase_order->total_amount = 0;
        $purchase_order->associate_id = Input::get('slct_associate');
        $purchase_order->sponsor_id = Input::get('slct_sponsor');
        $purchase_order->consultant_id = Input::get('slct_consultant');
        $purchase_order->fast_track = Input::get('is_fast_track');
        $purchase_order->status = Input::get('slct_trans_status');
        $purchase_order->remarks = Input::get('txtarea_remarks');
        $purchase_order->client_client_id = 1;
        $purchase_order->user_user_id = 1;
        $purchase_order->transaction_status = 'Approved';
        $purchase_order->save();
    }
}
