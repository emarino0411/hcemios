<?php

namespace App\Http\Controllers;

use App\Booking;
use App\BookingRepresentative;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class BookingsController extends Controller
{
    public function calendarView()
    {
        $bookings = DB::table('bookings')
                ->select(DB::raw('id,
                                      concat("#",id,":",event_name) as title,
                                      booking_date,
                                      concat(booking_date," ",booking_time) as start,
                                      concat(booking_date," ",booking_time_end) as end,
                                      color as backgroundColor,
                                      color as borderColor
                                      '))
                ->get();

        return view('bookings/bookings_calendar', [
                'page_title' => 'HCEMIOS Bookings',
                'page_description' => 'Where are we having our BOM today? :)',
                'level' => 'Bookings',
                'sub_level' => 'Calendar',
                'icon' => 'fa-calendar',
                'bookings' => $bookings
        ]);
    }

    public function create()
    {
        $booking = new Booking();
        $booking->event_name = Input::get('event_name');
        $booking->street = Input::get('street');
        $booking->city = Input::get('city');
        $booking->province = Input::get('province');
        $booking->status = 'Pending';
        $booking->client = Input::get('client');
        $booking->color = Input::get('color');
        $booking->save();

        $booking_rep = new BookingRepresentative();
        $booking_rep->booking_id = $booking->id;
        $booking_rep->representative = Input::get('representative');
        $booking_rep->save();

        return '#' . $booking->id . ': ' . $booking->event_name;
    }

    public function updateDate()
    {
        $booking = Booking::find(Input::get('id'));
        $booking->booking_date = Input::get('booking_date');
        $booking->booking_time = Input::get('booking_time');
        $booking->booking_time_end = Input::get('booking_time_end');
        $booking->update();
    }

    public function getBookingDetails()
    {
        $booking_id = Input::get('bookingId');
        $booking = DB::table('bookings')
                ->leftJoin('booking_reps', 'bookings.id', '=', 'booking_reps.booking_id')
                ->where('bookings.id', '=', $booking_id)
                ->get();

        return $booking;
    }

    public function cancelBooking()
    {
        $booking = Booking::find(Input::get('bookingId'));
        $booking_rep = BookingRepresentative::where('booking_id', '=', $booking->id);

        $booking->delete();
        $booking_rep->delete();
    }
}
