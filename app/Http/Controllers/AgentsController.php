<?php

namespace App\Http\Controllers;

use App\Agent;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class AgentsController extends Controller
{
  //
  public function index()
  {
    $agents = Agent::all()->toArray();

    $page_title = 'HCEMIOS Agents';
    $page_description = 'The hardworking agents of Saladmaster :)';
    $level = 'Agents';
    $sub_level = 'List';
    $icon = 'fa fa-users';

    return view('agents/agents_list', compact('agents', 'page_title', 'page_description', 'level', 'sub_level', 'icon', 'roles'));
  }

  public function addAgent()
  {
    $page_title = 'HCEMIOS Agents';
    $page_description = 'Add an Agent';
    $level = 'Agents';
    $sub_level = 'Add New';
    $icon = 'fa fa-users';

    $municipalities = DB::table('rcitymun')
      ->join('rprov', 'rcitymun.provcode', '=', 'rprov.provcode')
      ->select(DB::raw('concat(cityname," [",provname,"]") as cityname'), 'rcitymun.citycode')
      ->orderBy('cityname', 'asc')
      ->orderBy('provname', 'asc')
      ->get();

    $provinces = DB::table('rprov')
      ->select('provcode', 'provname')
      ->orderBy('provname', 'asc')
      ->get();

    return view('agents/agents_add', compact('page_title','municipalities','provinces', 'page_description', 'level', 'sub_level', 'icon', 'municipalities', 'provinces'));
  }

  public function create()
  {
    $agent = new Agent();
    $agent->last_name = Input::get('txt_last_name');
    $agent->first_name = Input::get('txt_first_name');
    $agent->representative_code = substr($agent->first_name, 0, 1) . $agent->last_name;
    $agent->sponsor_id = Input::get('txt_agent_sponsor');
    $agent->sponsor_date = Input::get('txt_agent_sponsor_date');
    $agent->birthdate = Input::get('txt_agent_birthdate');
    $agent->civil_status = Input::get('slct_civil_status');
    $agent->street = Input::get('txt_street');
    $agent->brgy = Input::get('txt_brgy');
    $agent->city = Input::get('slct_town');
    $agent->province = Input::get('slct_province');
    $agent->gender = Input::get('rdo_gender');
    $agent->phone1 = Input::get('txt_agent_landline');
    $agent->phone2 = Input::get('txt_agent_mobile');
    $agent->tin_no = Input::get('txt_tin');
    $agent->sss_no = Input::get('txt_sss');
    $agent->philhealth_no = Input::get('txt_philhealth');
    $agent->pagibig_no = Input::get('txt_pagibig');
    $agent->can_drive = Input::get('rdo_can_drive');
    $agent->own_car = Input::get('rdo_own_car');
    $agent->previous_employer = Input::get('txt_employer_name');
    $agent->p_employer_telno = Input::get('txt_employer_contact');
    $agent->p_employer_addr = Input::get('txt_employer_address');
    $agent->p_employer_email = Input::get('txt_employer_email');
    $agent->spouse_last_name = Input::get('txt_spouse_last_name');
    $agent->spouse_first_name = Input::get('txt_spouse_first_name');
    $agent->no_of_dependents = Input::get('slct_dependents');
    $agent->email_address = Input::get('txt_agent_email');
    $agent->save();
  }

  public function viewProfile($id, $edit_mode = '')
  {
    $agent = Agent::find($id);

    $page_title = 'HCEMIOS Agents';
    $page_description = 'View Agent Details';
    $level = 'Agents';
    $sub_level = 'View Profile';
    $icon = 'fa fa-users';

    $municipalities = DB::table('rcitymun')
      ->join('rprov', 'rcitymun.provcode', '=', 'rprov.provcode')
      ->select(DB::raw('concat(cityname," [",provname,"]") as cityname'), 'provname', 'rcitymun.citycode')
      ->orderBy('cityname', 'asc')
      ->orderBy('provname', 'asc')
      ->get();

    $provinces = DB::table('rprov')
      ->select('provcode', 'provname')
      ->orderBy('provname', 'asc')
      ->get();

    // set view
    return view('agents/agents_profile', compact('provinces', 'municipalities', 'agent', 'page_title',
      'page_description', 'level', 'sub_level', 'icon', 'edit_mode'));
  }

  public function editAgent()
  {

  }

  public function update()
  {
    $agent = Agent::find(Input::get('txt_agent_id'));
    $agent->last_name = Input::get('txt_last_name');
    $agent->first_name = Input::get('txt_first_name');
    $agent->representative_code = substr($agent->first_name, 0, 1) . $agent->last_name;
    $agent->sponsor_id = Input::get('txt_agent_sponsor');
    $agent->sponsor_date = Input::get('txt_agent_sponsor_date');
    $agent->birthdate = Input::get('txt_agent_birthdate');
    $agent->civil_status = Input::get('slct_civil_status');
    $agent->street = Input::get('txt_street');
    $agent->brgy = Input::get('txt_brgy');
    $agent->city = Input::get('slct_town');
    $agent->province = Input::get('slct_province');
    $agent->gender = Input::get('rdo_gender');
    $agent->phone1 = Input::get('txt_agent_landline');
    $agent->phone2 = Input::get('txt_agent_mobile');
    $agent->tin_no = Input::get('txt_tin');
    $agent->sss_no = Input::get('txt_sss');
    $agent->philhealth_no = Input::get('txt_philhealth');
    $agent->pagibig_no = Input::get('txt_pagibig');
    $agent->can_drive = Input::get('rdo_can_drive');
    $agent->own_car = Input::get('rdo_own_car');
    $agent->previous_employer = Input::get('txt_employer_name');
    $agent->p_employer_telno = Input::get('txt_employer_contact');
    $agent->p_employer_addr = Input::get('txt_employer_address');
    $agent->p_employer_email = Input::get('txt_employer_email');
    $agent->spouse_last_name = Input::get('txt_spouse_last_name');
    $agent->spouse_first_name = Input::get('txt_spouse_first_name');
    $agent->no_of_dependents = Input::get('slct_dependents');
    $agent->email_address = Input::get('txt_agent_email');
    $agent->save();
  }

  public function delete()
  {
    $agent = Agent::find(Input::get('userId'));
    $agent->status = 'DEACTIVATED';
    $agent->update();
    if ($agent->update()) {
      return 'USER_DELETED';
    } else {
      return 'Error';
    }
  }

  public function activate()
  {
    $agent = Agent::find(Input::get('userId'));
    $agent->status = 'ACTIVATED';
    $agent->update();
    if ($agent->update()) {
      return 'USER_ACTIVATED';
    } else {
      return 'Error';
    }
  }
}
