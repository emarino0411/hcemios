<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Role;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class UsersController extends Controller
{
  public function index()
  {
    $users = User::all()->toArray();

    $page_title = 'HCEMIOS Users';
    $page_description = 'We Control Everything';
    $level = 'Users';
    $sub_level = 'List';
    $icon = 'fa fa-street-view';

    return view('users/users_list', compact('users', 'page_title', 'page_description', 'level', 'sub_level', 'icon', 'roles'));
  }

  public function addUser()
  {
    $page_title = 'HCEMIOS Users';
    $page_description = 'Adding another User?';
    $level = 'Users';
    $sub_level = 'Add New';
    $icon = 'fa fa-street-view';

    $roles = Role::all()->toArray();

    $municipalities = DB::table('rcitymun')
      ->join('rprov', 'rcitymun.provcode', '=', 'rprov.provcode')
      ->select(DB::raw('concat(cityname," [",provname,"]") as cityname'), 'rcitymun.provcode')
      ->orderBy('cityname', 'asc')
      ->orderBy('provname', 'asc')
      ->get();

    $provinces = DB::table('rprov')
      ->select('provcode', 'provname')
      ->orderBy('provname', 'asc')
      ->get();
    return view('users/users_add', compact('roles', 'brgy', 'municipalities', 'provinces',
      'page_title', 'page_description', 'level', 'sub_level', 'icon', 'roles'));
  }

  public function viewProfile($id, $edit_mode = '')
  {
    // get user details
    $roles = Role::all()->toArray();
    $user = User::find($id);

    $page_title = 'HCEMIOS Users';
    $page_description = 'Checking out our Users?';
    $level = 'Users';
    $sub_level = 'View Profile:' . $user->first_name . ' ' . $user->last_name;
    $icon = 'fa fa-street-view';

    // get user role name
    $user_role = Role::find($user->user_role)->get()->first();
    $user->user_role = $user_role->name;

    $municipalities = DB::table('rcitymun')
      ->join('rprov', 'rcitymun.provcode', '=', 'rprov.provcode')
      ->select(DB::raw('concat(cityname," [",provname,"]") as cityname'), 'provname')
      ->orderBy('cityname', 'asc')
      ->orderBy('provname', 'asc')
      ->get();

    $provinces = DB::table('rprov')
      ->select('provcode', 'provname')
      ->orderBy('provname', 'asc')
      ->get();

    // set view
    return view('users/users_profile', compact('provinces', 'municipalities', 'user', 'page_title',
      'page_description', 'level', 'sub_level', 'icon', 'roles', 'edit_mode'));
  }

  public function create()
  {
    $is_email_duplicate = User::where('email', '=', Input::get('user_email'))->count();
    if ($is_email_duplicate > 0) {
      return 'EMAIL_EXISTING';
    }
    $user = new User();
    $user->email = Input::get('user_email');
    $user->password = bcrypt(Input::get('user_password'));
    $user->last_name = Input::get('user_last_name');
    $user->first_name = Input::get('user_first_name');
    $user->gender = Input::get('user_gender');
    $user->civil_status = Input::get('user_civil_status');
    $user->birthdate = Input::get('user_birthdate');
    $user->landline = Input::get('user_landline');
    $user->mobile = Input::get('user_mobile');
    $user->street = Input::get('user_street');
    $user->brgy = Input::get('user_brgy');
    $user->city = Input::get('user_town');
    $user->province = Input::get('user_province');
    $user->user_role = Input::get('user_role');
    $user->status = 'ACTIVE';
    if ($user->save()) {
      return 'USER_SAVED';
    } else {
      return 'data';
    }
  }

  public function update()
  {
    $get_user_id = User::where('email', '=', Input::get('old_email'))->get()->first();

    $user = User::find($get_user_id->id);

    $is_email_duplicate = User::where('email', '=', Input::get('user_email'))
      ->where('id', '!=', $get_user_id->id)
      ->count();

    if ($is_email_duplicate > 0) {
      return 'EMAIL_EXISTING';
    }
    $user->email = Input::get('user_email');
    $user->last_name = Input::get('user_last_name');
    $user->first_name = Input::get('user_first_name');
    $user->gender = Input::get('user_gender');
    $user->civil_status = Input::get('user_civil_status');
    $user->birthdate = Input::get('user_birthdate');
    $user->landline = Input::get('user_landline');
    $user->mobile = Input::get('user_mobile');
    $user->street = Input::get('user_street');
    $user->brgy = Input::get('user_brgy');
    $user->city = Input::get('user_town');
    $user->province = Input::get('user_province');
    $user->user_role = Input::get('user_role');
    $user->update();
    if ($user->update()) {
      return 'USER_UPDATED';
    } else {
      return 'Error';
    }

  }

  public function delete()
  {
    $user = User::find(Input::get('userId'));
    $user->status = 'DEACTIVATED';
    $user->update();
    if ($user->update()) {
      return 'USER_DELETED';
    } else {
      return 'Error';
    }
  }

  public function activate()
  {
    $user = User::find(Input::get('userId'));
    $user->status = 'ACTIVE';
    $user->update();
    if ($user->update()) {
      return 'USER_ACTIVATED';
    } else {
      return 'Error';
    }
  }

  public function viewLogs()
  {

  }
}
