<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class ClientsController extends Controller
{
  public function index()
  {
    $clients = Client::all();
    $page_title = 'HCEMIOS Clients';
    $page_description = 'Let us talk about Clients :)';
    $level = 'Clients';
    $sub_level = 'List';
    $icon = 'fa fa-briefcase';

    return view('clients/clients_list', compact('clients', 'municipalities', 'provinces', 'page_title', 'page_description', 'level', 'sub_level', 'icon', 'roles'));
  }

  public function addNew()
  {
    $page_title = 'HCEMIOS Clients';
    $page_description = 'Add a New Client :)';
    $level = 'Clients';
    $sub_level = 'Add New Client';
    $icon = 'fa fa-briefcase';

    $municipalities = DB::table('rcitymun')
      ->join('rprov', 'rcitymun.provcode', '=', 'rprov.provcode')
      ->select(DB::raw('concat(cityname," [",provname,"]") as cityname'), 'rcitymun.citycode')
      ->orderBy('cityname', 'asc')
      ->orderBy('provname', 'asc')
      ->get();

    $provinces = DB::table('rprov')
      ->select('provcode', 'provname')
      ->orderBy('provname', 'asc')
      ->get();

    return view('clients/client_add', compact('municipalities', 'provinces', 'page_title', 'page_description', 'level',
      'sub_level', 'icon', 'roles'));
  }

  public function create()
  {
    $client = new Client();
    $client->last_name = Input::get('txt_last_name');
    $client->first_name = Input::get('txt_first_name');
    $client->street = Input::get('txt_street');
    $client->civil_status = Input::get('slct_civil_status');
    $client->gender = Input::get('rdo_gender');
    $client->brgy = Input::get('txt_brgy');
    $client->city = Input::get('slct_town');
    $client->province = Input::get('slct_province');
    $client->phone1 = Input::get('txt_landline');
    $client->phone2 = Input::get('txt_mobile');
    $client->email_address = Input::get('txt_email');
    $client->valid_id_no = Input::get('txt_id_no');
    $client->valid_id_type = Input::get('txt_id_type');
    $client->business_name = Input::get('txt_business_name');
    $client->business_street = Input::get('txt_business_street');
    $client->business_brgy = Input::get('slct_business_brgy');
    $client->business_city = Input::get('slct_business_town');
    $client->business_province = Input::get('slct_business_province');
    $client->business_landline = Input::get('txt_business_landline');
    $client->business_mobile = Input::get('txt_business_mobile');
    $client->business_email = Input::get('txt_business_email');
    $client->occupation = Input::get('txt_occupation');
    $client->birthdate = Input::get('txt_birthdate');
    $client->valid_signature = "";
    $client->status = 'Active';
    $client->save();
    if ($client->save()) {
      return 'CLIENT_SAVED';
    } else {
      return 'ERROR';
    }

  }

  public function viewProfile($id, $edit_mode = '')
  {
    $client = Client::find($id);
    $page_title = 'HCEMIOS Clients';
    $page_description = 'More about this Client:)';
    $level = 'Clients';
    $sub_level = 'View Client Details';
    $icon = 'fa fa-briefcase';

    $municipalities = DB::table('rcitymun')
      ->join('rprov', 'rcitymun.provcode', '=', 'rprov.provcode')
      ->select(DB::raw('concat(cityname," [",provname,"]") as cityname'), 'rcitymun.citycode')
      ->orderBy('cityname', 'asc')
      ->orderBy('provname', 'asc')
      ->get();

    $provinces = DB::table('rprov')
      ->select('provcode', 'provname')
      ->orderBy('provname', 'asc')
      ->get();

    return view('clients/client_profile', compact('client', 'municipalities', 'provinces', 'page_title', 'page_description',
      'level', 'sub_level', 'icon', 'roles', 'edit_mode'));
  }

  public function update()
  {
    $client = Client::find(Input::get('client_id'));
    $client->last_name = Input::get('txt_last_name');
    $client->first_name = Input::get('txt_first_name');
    $client->street = Input::get('txt_street');
    $client->civil_status = Input::get('slct_civil_status');
    $client->gender = Input::get('rdo_gender');
    $client->brgy = Input::get('txt_brgy');
    $client->city = Input::get('slct_town');
    $client->province = Input::get('slct_province');
    $client->phone1 = Input::get('txt_landline');
    $client->phone2 = Input::get('txt_mobile');
    $client->email_address = Input::get('txt_email');
    $client->valid_id_no = Input::get('txt_id_no');
    $client->valid_id_type = Input::get('txt_id_type');
    $client->business_name = Input::get('txt_business_name');
    $client->business_street = Input::get('txt_business_street');
    $client->business_brgy = Input::get('slct_business_brgy');
    $client->business_city = Input::get('slct_business_town');
    $client->business_province = Input::get('slct_business_province');
    $client->business_landline = Input::get('txt_business_landline');
    $client->business_mobile = Input::get('txt_business_mobile');
    $client->business_email = Input::get('txt_business_email');
    $client->occupation = Input::get('txt_occupation');
    $client->birthdate = Input::get('txt_birthdate');
    $client->valid_signature = "";
    $client->status = 'Active';
    $client->save();
    if ($client->save()) {
      return 'CLIENT_SAVED';
    } else {
      return 'ERROR';
    }
  }

  public function delete()
  {
    $client = Client::find(Input::get('client_id'));
    $client->status = 'Deactivated';
    $client->save();
    if ($client->save()) {
      return 'CLIENT_DELETED';
    } else {
      return 'ERROR';
    }
  }
  public function activate()
  {
    $client = Client::find(Input::get('client_id'));
    $client->status = 'Active';
    $client->save();
    if ($client->save()) {
      return 'CLIENT_ACTIVE';
    } else {
      return 'ERROR';
    }
  }

}
