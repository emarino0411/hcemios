<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class RefAddressController extends Controller
{
  public function loadProvince()
  {
    $selected_province = DB::table('rcitymun')->where('citycode', '=', Input::get('citycode'))->first()->provcode;
//    $provinces = DB::table('rprov')
//      ->select('provcode', 'provname',DB::raw("if(provcode=".$selected_province.",'selected','') as status"))
//      ->orderBy('provname', 'asc')
//      ->get();
    return $selected_province;
  }

  public function loadMunicipality()
  {
    if (Input::get('provcode') == "") {
      $municipalities = DB::table('rcitymun')
        ->join('rprov', 'rcitymun.provcode', '=', 'rprov.provcode')
        ->select(DB::raw('concat(cityname," [",provname,"]") as cityname'), 'provname')
        ->orderBy('cityname', 'asc')
        ->orderBy('provname', 'asc')
        ->get();
    } else {
      $municipalities = DB::table('rcitymun')
        ->join('rprov', 'rcitymun.provcode', '=', 'rprov.provcode')
        ->select(DB::raw('concat(cityname," [",provname,"]") as cityname'), 'provname')
        ->where('rprov.provcode', '=', Input::get('provcode'))
        ->orderBy('cityname', 'asc')
        ->orderBy('provname', 'asc')
        ->get();
    }

    return $municipalities;
  }
}
