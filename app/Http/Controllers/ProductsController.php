<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;


class ProductsController extends Controller
{
    public function index()
    {
        $page_title = 'HCEMIOS Products';
        $page_description = 'Products and Sets';
        $level = 'Products and Sets';
        $sub_level = 'List';
        $icon = 'fa fa-cutlery';

        $products = Product::all();

        return view('products/products_list', compact('products', 'page_title',
                'page_description', 'level', 'sub_level', 'icon', 'roles', 'edit_mode'));
    }

    public function addProduct()
    {
        $page_title = 'HCEMIOS Products';
        $page_description = 'Products and Sets';
        $level = 'Products and Sets';
        $sub_level = 'Add New Products';
        $icon = 'fa fa-cutlery';


        return view('products/products_add', compact('page_title',
                'page_description', 'level', 'sub_level', 'icon', 'edit_mode'));

    }

    public function create()
    {
        $product = new Product();
        $product->item_code = Input::get('item_code');
        $product->description = Input::get('description');
        $product->retail_price = Input::get('retail_price');
        $product->inventory_price = Input::get('inventory_price');
        $product->category = Input::get('category');
        $product->type = Input::get('product_type');
        $product->product_type = Input::get('product_type');
        $product->save();
        $this->index();
    }

    public function viewProduct($id, $edit_mode = '')
    {
        $page_title = 'HCEMIOS Products';
        $page_description = 'Just checking out our products';
        $level = 'Products and Sets';
        $sub_level = 'View Product Details';
        $icon = 'fa fa-cutlery';
        $product = Product::find($id)->first();

        return view('products/products_profile', compact('product', 'page_title',
                'page_description', 'level', 'sub_level', 'icon', 'edit_mode'));
    }

    public function update()
    {
        $product = Product::find(Input::get('id'));
        $product->item_code = Input::get('item_code');
        $product->description = Input::get('description');
        $product->retail_price = Input::get('retail_price');
        $product->inventory_price = Input::get('inventory_price');
        $product->category = Input::get('category');
        $product->type = Input::get('product_type');
        $product->save();
    }

    public function delete()
    {
        $user = Product::find(Input::get('productId'));
        $user->status = 'DEACTIVATED';
        $user->update();
        if ($user->update()) {
            return 'PRODUCT_DELETED';
        } else {
            return 'Error';
        }
    }

    public function activate()
    {
        $user = Product::find(Input::get('productId'));
        $user->status = 'ACTIVE';
        $user->update();
        if ($user->update()) {
            return 'PRODUCT_ACTIVATED';
        } else {
            return 'Error';
        }
    }

    public function getProductsByType(){
        if(Input::get('product_type')=='item')
            return $products = Product::where('type','=','Single')->get();
        if(Input::get('product_type')=='set')
            return $products = Product::where('type','=','Set')->get();

    }


}
