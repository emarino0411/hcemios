<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'item';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['item_code', 'description', 'retail_price', 'inventory_price', 'production_count',
    'production_count_FT', 'category','product_type'];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [];
}
