<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'client';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['last_name', 'first_name', 'street', 'city', 'province', 'phone1', 'phone2', 'email_address',
    'valid_id_no', 'valid_id_type','business_name','business_address','business_phone','valid_signature'];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [];
}
