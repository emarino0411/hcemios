<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingRepresentative extends Model
{
  protected $table = 'booking_reps';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = ['representative','booking_id'];

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [];
}
